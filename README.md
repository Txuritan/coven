# Coven

A bookmark and soon to be rss manager based off [Shiori](https://github.com/RadhiFadlillah/shiori).

## TODO

- [ ] Rewrite readability into Rust so we can removed the Go server
- [ ] **AND OR** create an [WARC](https://en.wikipedia.org/wiki/Web_ARChive) crate
  - [ ] YouTube-DL for videos?
- [ ] Front end client
- [ ] Use [Siren](https://github.com/kevinswiber/siren) as API responses
  - [X] ~~[vnd_siren](https://crates.io/crates/vnd_siren) created for use with Coven~~
