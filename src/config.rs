use std::borrow::Cow;

use config::{Config, ConfigError, Environment, File};

#[derive(Debug, Clone, Serialize, Deserialize, SmartDefault)]
pub(crate) struct Settings {
    #[default(Default::default())]
    pub(crate) database: Database,
    #[default(Default::default())]
    pub(crate) server: Server,
    #[default(Default::default())]
    pub(crate) url2json: Url2Json,
}

#[derive(Debug, Clone, Serialize, Deserialize, SmartDefault)]
pub(crate) struct Database {
    #[default(Cow::from("coven.db"))]
    pub(crate) file: Cow<'static, str>,
}

#[derive(Clone, Debug, Deserialize, Serialize, SmartDefault)]
pub(crate) struct Server {
    #[default(Cow::from("127.0.0.1"))]
    pub(crate) host: Cow<'static, str>,
    #[default(8600)]
    pub(crate) port: u16,
    pub(crate) tls: Option<Tls>,
}

#[derive(Clone, Debug, Deserialize, Serialize, SmartDefault)]
pub(crate) struct Tls {
    pub(crate) cert: Cow<'static, str>,
    pub(crate) key: Cow<'static, str>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub(crate) struct Url2Json {
    pub(crate) binary: Cow<'static, str>,
}

impl Default for Url2Json {
    fn default() -> Url2Json {
        Url2Json {
            binary: Cow::from(if cfg!(target_os = "windows") {
                ".\\coven-url2json.exe"
            } else {
                "./coven-url2json"
            }),
        }
    }
}

impl Settings {
    pub(crate) fn load() -> Result<Self, ConfigError> {
        let mut s = Config::try_from(&Settings::default())?;

        let _ = s.merge(File::with_name("Coven").required(false))?;

        let _ = s.merge(Environment::with_prefix("coven"))?;

        s.try_into()
    }
}
