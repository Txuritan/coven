#![deny(
    single_use_lifetimes,
    // trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications,
    unreachable_pub,
    unused_results
)]

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
#[macro_use]
extern crate smart_default;
#[macro_use]
extern crate serde_derive;

#[macro_use]
mod slog;

mod commands;
mod models;
mod routes;

mod config;
mod database;
mod error;
mod sqlite;

use std::process::Command;

use actix_web::server;
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

use crate::{config::Settings, database::Database, routes::*};

pub(crate) struct Coven {
    pub(crate) database: actix_web::actix::Addr<Database>,
    pub(crate) settings: Settings,
}

fn main() -> Result<(), Box<std::error::Error>> {
    std::env::set_var("RUST_LOG", "info, actix_web=info");

    run()?;

    Ok(())
}

fn run() -> Result<(), Box<std::error::Error>> {
    use crate::slog::Drain;

    let drain = crate::slog::async_::Async::default(
        crate::slog::term_::CompactFormat::new(crate::slog::term_::TermDecorator::new().build())
            .build()
            .fuse(),
    );

    let logger = crate::slog::Logger::root(drain.fuse(), o!());

    let _scope_guard = crate::slog::scope_::set_global_logger(logger);
    crate::slog::stdlog_::init()?;

    let settings = Settings::load()?;

    let builder = if let Some(tls) = &settings.server.tls {
        let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls())?;
        builder.set_private_key_file(&tls.key.to_string(), SslFiletype::PEM)?;
        builder.set_certificate_chain_file(&tls.cert.to_string())?;

        Some(builder)
    } else {
        None
    };

    let db = Database::load(&settings);

    info!("Starting Go server");
    let mut child = Command::new(format!("{}", &settings.url2json.binary))
        .arg(format!("{}", &settings.server.port + 1))
        .spawn()?;

    let address = format!("{}:{}", &settings.server.host, &settings.server.port);

    let sys = actix::System::new("coven");

    let server = server::new(move || vec![app(db.clone(), settings.clone())]);

    let _ = if let Some(builder) = builder {
        server.bind_ssl(&address, builder)?
    } else {
        server.bind(&address)?
    }
    .start();

    let _ = sys.run();

    child.kill()?;
    info!("Stopped Go Server");

    Ok(())
}
