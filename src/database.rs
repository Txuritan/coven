use actix_web::actix::*;
use r2d2::Pool;
use rusqlite::NO_PARAMS;

use crate::{config::Settings, sqlite::SqliteConnectionManager};

const TABLES: [(&str, &str); 5] = [
    ("bookmarks", include_str!("sql/bookmarks.sql")),
    ("folders", include_str!("sql/folders.sql")),
    ("tags", include_str!("sql/tags.sql")),
    ("bookmark_tags", include_str!("sql/bookmark_tags.sql")),
    ("bookmark_content", include_str!("sql/bookmark_content.sql")),
];

pub(crate) struct Database {
    pub(crate) pool: Pool<SqliteConnectionManager>,
}

impl Actor for Database {
    type Context = SyncContext<Self>;
}

impl Clone for Database {
    fn clone(&self) -> Database {
        Database {
            pool: self.pool.clone(),
        }
    }
}

impl Database {
    pub(crate) fn load(settings: &Settings) -> Database {
        use std::error::Error;

        let manager = SqliteConnectionManager::file(settings.database.file.to_string());

        let pool = match Pool::new(manager) {
            Ok(p) => p,
            Err(err) => {
                error!("R2D2 Pool Error: {} {:?}", err.description(), err.cause());

                ::std::process::exit(-1);
            }
        };

        {
            let run = || -> Result<(), Box<Error>> {
                let mut conn = pool.get()?;
                let tx = conn.transaction()?;

                for table in &TABLES {
                    trace!("Running SQL: {}", table.0);

                    let _ = tx.execute(table.1, NO_PARAMS)?;
                }

                tx.commit()?;

                Ok(())
            };

            match run() {
                Ok(_) => {}
                Err(err) => {
                    error!(
                        "Database Setup Error: {} {:?}",
                        err.description(),
                        err.cause()
                    );

                    ::std::process::exit(-1);
                }
            }
        }

        Database { pool }
    }
}
