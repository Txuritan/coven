CREATE TABLE
IF NOT EXISTS
    bookmarks (
        id              INTEGER     NOT NULL    PRIMARY KEY,
        title           TEXT        NOT NULL,
        url             TEXT        NOT NULL    UNIQUE,
        author          TEXT        NOT NULL    DEFAULT "",
        excerpt         TEXT        NOT NULL    DEFAULT "",
        min_read_time   INTEGER     NOT NULL    DEFAULT 0,
        max_read_time   INTEGER     NOT NULL    DEFAULT 0,
        modified        TEXT        NOT NULL    DEFAULT CURRENT_TIMESTAMP,
        folder          INTEGER     NOT NULL    DEFAULT 0
    );
