CREATE VIRTUAL TABLE
IF NOT EXISTS
    bookmark_content
USING
    fts4 (
        title,
        content,
        html
    );
