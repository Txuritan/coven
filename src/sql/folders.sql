CREATE TABLE
IF NOT EXISTS
    folders (
        id              INTEGER     NOT NULL    PRIMARY KEY,
        name            TEXT        NOT NULL,
        parent          INTEGER     NOT NULL    DEFAULT 0
    );
