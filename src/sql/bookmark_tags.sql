CREATE TABLE
IF NOT EXISTS
    bookmark_tags (
        id              INTEGER     NOT NULL    PRIMARY KEY,
        bookmark_id     INTEGER     NOT NULL,
        tag_id          INTEGER     NOT NULL,

        CONSTRAINT bookmark_id_FK
            FOREIGN KEY (bookmark_id)
            REFERENCES bookmarks (id),
        CONSTRAINT tag_id_FK
            FOREIGN KEY (tag_id)
            REFERENCES tags (id)
    );
