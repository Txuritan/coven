CREATE TABLE
IF NOT EXISTS
    tags (
        id              INTEGER     NOT NULL    PRIMARY KEY,
        name            TEXT        NOT NULL    UNIQUE
    );
