use std::borrow::Cow;

use rusqlite::{types::ToSql, Connection, OptionalExtension};
use serde_derive::{Deserialize, Serialize};

use crate::{error::Error, models::go};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct Bookmark {
    id: i64,
    title: String,
    url: String,
    author: String,
    excerpt: String,
    min_read_time: i64,
    max_read_time: i64,

    #[serde(skip_serializing_if = "Option::is_none")]
    content: Option<BookmarkContent>,
    tags: Vec<Tag>,
}

impl Bookmark {
    pub(crate) fn create(
        conn: &Connection,
        url: &str,
        tags: &[&str],
        title: &Option<Cow<'static, str>>,
        res: go::GoResponse,
    ) -> Result<Bookmark, Error> {
        match res {
            go::GoResponse::Error(err) => Err(Error::GoResponse(err)),
            go::GoResponse::Article(ref article) => {
                match conn
                    .query_row(
                        "SELECT id FROM bookmarks WHERE url = ?1 LIMIT 1;",
                        &[&url],
                        |row| -> i64 { row.get("id") },
                    )
                    .optional()?
                {
                    Some(id) => {
                        let bookmark = Bookmark::get(&conn, id, true)?;

                        Ok(bookmark)
                    }
                    None => {
                        let title = if let Some(title) = title {
                            title.to_string()
                        } else {
                            article.meta.title.to_string()
                        };

                        let _ = conn.execute(
                            "INSERT INTO bookmarks (title, url, author, excerpt, min_read_time, max_read_time) VALUES (?1, ?2, ?3, ?4, ?5, ?6);",
                            &[
                                &title as &ToSql,
                                &url,
                                &article.meta.author.to_string(),
                                &article.meta.excerpt.to_string(),
                                &article.meta.min_read_time,
                                &article.meta.max_read_time,
                            ]
                        )?;

                        let bookmark_id = conn.last_insert_rowid();

                        let mut bookmark = Bookmark {
                            id: bookmark_id,
                            title,
                            url: url.to_string(),
                            author: article.meta.author.to_string(),
                            excerpt: article.meta.excerpt.to_string(),
                            min_read_time: article.meta.min_read_time.into(),
                            max_read_time: article.meta.max_read_time.into(),

                            content: Some(BookmarkContent::create(
                                &conn,
                                bookmark_id,
                                &article.meta.title,
                                &article.content,
                                &article.raw_content,
                            )?),
                            tags: Vec::new(),
                        };

                        for tag in tags {
                            let tag = Tag::create(&conn, tag)?;

                            let _ = BookmarkTag::create(&conn, bookmark_id, tag.id)?;

                            bookmark.tags.push(tag);
                        }

                        Ok(bookmark)
                    }
                }
            }
        }
    }

    pub(crate) fn get(conn: &Connection, id: i64, content: bool) -> Result<Bookmark, Error> {
        let mut bookmark = if content {
            let (_id, title, url, author, excerpt, min_read_time, max_read_time, content_id, content, raw_content) = conn.query_row(
                "SELECT b.id, b.title, b.url, b.author, b.excerpt, b.min_read_time, b.max_read_time, c.rowid, c.content, c.html FROM bookmarks b LEFT JOIN bookmark_content c ON c.docid = b.id WHERE b.id = ?1 LIMIT 1;",
                &[&id],
                |row| -> (
                    i64, String, String, String, String, i64, i64,
                    i64, String, String
                ) {
                    (
                        row.get("id"),
                        row.get("title"),
                        row.get("url"),
                        row.get("author"),
                        row.get("excerpt"),
                        row.get("min_read_time"),
                        row.get("max_read_time"),

                        row.get("rowid"),
                        row.get("content"),
                        row.get("html"),
                    )
                }
            )?;

            Bookmark {
                id,
                title,
                url,
                author,
                excerpt,
                min_read_time,
                max_read_time,

                content: Some(BookmarkContent {
                    id: content_id,
                    content,
                    raw_content,
                }),
                tags: Vec::new(),
            }
        } else {
            let (_id, title, url, author, excerpt, min_read_time, max_read_time) = conn.query_row(
                "SELECT b.id, b.title, b.url, b.author, b.excerpt, b.min_read_time, b.max_read_time FROM bookmarks b WHERE b.id = ?1 LIMIT 1;",
                &[&id],
                |row| -> (
                    i64, String, String, String, String, i64, i64
                ) {
                    (
                        row.get("id"),
                        row.get("title"),
                        row.get("url"),
                        row.get("author"),
                        row.get("excerpt"),
                        row.get("min_read_time"),
                        row.get("max_read_time"),
                    )
                }
            )?;

            Bookmark {
                id,
                title,
                url,
                author,
                excerpt,
                min_read_time,
                max_read_time,

                content: None,
                tags: Vec::new(),
            }
        };

        let mut tags_stmt = conn.prepare("SELECT t.id, t.name FROM bookmark_tags bt LEFT JOIN tags t ON bt.tag_id = t.id WHERE bt.bookmark_id = ?1 ORDER BY t.name;")?;

        let tags = tags_stmt.query_map(&[&id], |row| -> (i64, String) {
            (row.get("id"), row.get("name"))
        })?;

        for tag in tags {
            let (id, name) = tag?;

            bookmark.tags.push(Tag { id, name });
        }

        Ok(bookmark)
    }

    pub(crate) fn get_all(
        conn: &Connection,
        offset: i64,
        mut limit: i64,
        content: bool,
    ) -> Result<Vec<Bookmark>, Error> {
        if limit > 100 {
            limit = 100;
        }

        let mut bookmark_stmt = conn.prepare(if content {
            "SELECT b.id, b.title, b.url, b.author, b.excerpt, b.min_read_time, b.max_read_time, c.rowid, c.content, c.html FROM bookmarks b LEFT JOIN bookmark_content c ON c.docid = b.id LIMIT ?1 OFFSET ?2;"
        } else {
            "SELECT b.id, b.title, b.url, b.author, b.excerpt, b.min_read_time, b.max_read_time FROM bookmarks b LIMIT ?1 OFFSET ?2;"
        })?;

        let mut tags_stmt = conn.prepare("SELECT t.id, t.name FROM bookmark_tags bt LEFT JOIN tags t ON bt.tag_id = t.id WHERE bt.bookmark_id = ?1 ORDER BY t.name;")?;

        let mut bookmarks = Vec::new();

        if content {
            let bookmark_query = bookmark_stmt.query_map(
                &[&limit, &offset],
                |row| -> (
                    i64,
                    String,
                    String,
                    String,
                    String,
                    i64,
                    i64,
                    i64,
                    String,
                    String,
                ) {
                    (
                        row.get("id"),
                        row.get("title"),
                        row.get("url"),
                        row.get("author"),
                        row.get("excerpt"),
                        row.get("min_read_time"),
                        row.get("max_read_time"),
                        row.get("rowid"),
                        row.get("content"),
                        row.get("html"),
                    )
                },
            )?;

            for bookmark in bookmark_query {
                let (
                    id,
                    title,
                    url,
                    author,
                    excerpt,
                    min_read_time,
                    max_read_time,
                    content_id,
                    content,
                    raw_content,
                ) = bookmark?;

                let mut bookmark = Bookmark {
                    id,
                    title,
                    url,
                    author,
                    excerpt,
                    min_read_time,
                    max_read_time,

                    content: Some(BookmarkContent {
                        id: content_id,
                        content,
                        raw_content,
                    }),
                    tags: Vec::new(),
                };

                let tags = tags_stmt.query_map(&[&id], |row| -> (i64, String) {
                    (row.get("id"), row.get("name"))
                })?;

                for tag in tags {
                    let (id, name) = tag?;

                    bookmark.tags.push(Tag { id, name });
                }

                bookmarks.push(bookmark);
            }
        } else {
            let bookmark_query = bookmark_stmt.query_map(
                &[&limit, &offset],
                |row| -> (i64, String, String, String, String, i64, i64) {
                    (
                        row.get("id"),
                        row.get("title"),
                        row.get("url"),
                        row.get("author"),
                        row.get("excerpt"),
                        row.get("min_read_time"),
                        row.get("max_read_time"),
                    )
                },
            )?;

            for bookmark in bookmark_query {
                let (id, title, url, author, excerpt, min_read_time, max_read_time) = bookmark?;

                let mut bookmark = Bookmark {
                    id,
                    title,
                    url,
                    author,
                    excerpt,
                    min_read_time,
                    max_read_time,

                    content: None,
                    tags: Vec::new(),
                };

                let tags = tags_stmt.query_map(&[&id], |row| -> (i64, String) {
                    (row.get("id"), row.get("name"))
                })?;

                for tag in tags {
                    let (id, name) = tag?;

                    bookmark.tags.push(Tag { id, name });
                }

                bookmarks.push(bookmark);
            }
        }

        Ok(bookmarks)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct BookmarkContent {
    id: i64,
    content: String,
    raw_content: String,
}

impl BookmarkContent {
    pub(crate) fn create(
        conn: &Connection,
        bookmark_id: i64,
        title: &str,
        content: &str,
        raw_content: &str,
    ) -> Result<BookmarkContent, Error> {
        let _ = conn.execute(
            "INSERT INTO bookmark_content (docid, title, content, html) VALUES (?1, ?2, ?3, ?4);",
            &[&bookmark_id as &ToSql, &title, &content, &raw_content],
        )?;

        Ok(BookmarkContent {
            id: conn.last_insert_rowid(),
            content: content.to_string(),
            raw_content: raw_content.to_string(),
        })
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct Folder {
    id: i64,
    name: String,
    parent: i64,
}

impl Folder {
    #[allow(dead_code)]
    pub(crate) fn create(
        conn: &Connection,
        name: &str,
        parent: Option<i64>,
    ) -> Result<Folder, Error> {
        match parent {
            Some(parent) => {
                if let Some((id, name, parent)) = conn
                    .query_row(
                        "SELECT id, name, parent FROM tags WHERE name = ?1 AND parent = ?2 LIMIT 1;",
                        &[&name as &ToSql, &parent],
                        |row| -> (i64, String, i64) {
                            (row.get("id"), row.get("name"), row.get("parent"))
                        },
                    )
                    .optional()?
                {
                    Ok(Folder { id, name, parent })
                } else {
                    let _ = conn.execute(
                        "INSERT INTO tags (name, parent) VALUES (?1, ?1);",
                        &[&name as &ToSql, &parent],
                    )?;

                    Ok(Folder {
                        id: conn.last_insert_rowid(),
                        name: name.to_string(),
                        parent,
                    })
                }
            }
            None => {
                if let Some((id, name, parent)) = conn
                    .query_row(
                        "SELECT id, name, parent FROM tags WHERE name = ?1 LIMIT 1;",
                        &[&name],
                        |row| -> (i64, String, i64) {
                            (row.get("id"), row.get("name"), row.get("parent"))
                        },
                    )
                    .optional()?
                {
                    Ok(Folder { id, name, parent })
                } else {
                    let _ = conn.execute("INSERT INTO tags (name) VALUES (?1);", &[&name])?;

                    Ok(Folder {
                        id: conn.last_insert_rowid(),
                        name: name.to_string(),
                        parent: 0,
                    })
                }
            }
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct Tag {
    id: i64,
    name: String,
}

impl Tag {
    pub(crate) fn create(conn: &Connection, tag: &str) -> Result<Tag, Error> {
        if let Some((id, name)) = conn
            .query_row(
                "SELECT id, name FROM tags WHERE name = ?1 LIMIT 1;",
                &[&tag],
                |row| -> (i64, String) { (row.get("id"), row.get("name")) },
            )
            .optional()?
        {
            Ok(Tag { id, name })
        } else {
            let _ = conn.execute("INSERT INTO tags (name) VALUES (?1);", &[&tag])?;

            Ok(Tag {
                id: conn.last_insert_rowid(),
                name: tag.to_string(),
            })
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct BookmarkTag {
    id: i64,
    bookmark_id: i64,
    tag_id: i64,
}

impl BookmarkTag {
    pub(crate) fn create(conn: &Connection, bookmark: i64, tag: i64) -> Result<i64, Error> {
        let _ = conn.execute(
            "INSERT OR IGNORE INTO bookmark_tags (bookmark_id, tag_id) VALUES (?1, ?2);",
            &[&bookmark, &tag],
        )?;

        Ok(conn.last_insert_rowid())
    }
}
