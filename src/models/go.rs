use std::borrow::Cow;
use std::str;

use bytes::Bytes;
use serde_derive::{Deserialize, Serialize};
use serde_json::from_str;

use crate::error::Error;

#[derive(Debug, Clone, Deserialize, Serialize)]
pub(crate) struct GoRequest {
    pub(crate) url: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(untagged)]
pub(crate) enum GoResponse {
    Article(Article),
    Error(Cow<'static, str>),
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub(crate) struct Article {
    pub(crate) content: Cow<'static, str>,
    pub(crate) meta: ArticleMeta,
    pub(crate) raw_content: Cow<'static, str>,
    #[serde(rename = "URL")]
    pub(crate) url: Cow<'static, str>,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub(crate) struct ArticleMeta {
    pub(crate) author: Cow<'static, str>,
    pub(crate) excerpt: Cow<'static, str>,
    pub(crate) image: Cow<'static, str>,
    pub(crate) max_read_time: i32,
    pub(crate) min_read_time: i32,
    pub(crate) title: Cow<'static, str>,
}

impl GoResponse {
    pub(crate) fn from_bytes(body: &Bytes) -> Result<GoResponse, Error> {
        let body = str::from_utf8(&body)?;

        let res = from_str(body)?;

        Ok(res)
    }
}
