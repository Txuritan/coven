/* cSpell:disable */

#![allow(
    dead_code,
    single_use_lifetimes,
// trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications,
    unreachable_pub,
    unused_must_use,
    unused_results,
    unused_variables,
    clippy::cast_lossless,
    clippy::clone_double_ref,
    clippy::collapsible_if,
    clippy::new_ret_no_self,
    clippy::no_effect,
    clippy::trivially_copy_pass_by_ref,
    clippy::type_complexity
)]

//! # Slog -  Structured, extensible, composable logging for Rust
//!
//! `slog-rs` is an ecosystem of reusable components for structured, extensible,
//! composable logging for Rust.
//!
//! `slog` is `slog-rs`'s main crate providing core components shared between
//! all other parts of `slog-rs` ecosystem.
//!
//! This is auto-generated technical documentation of `slog`. For information
//! about project organization, development, help, etc. please see
//! [slog github page](https://github.com/slog-rs/slog)
//!
//! ## Core advantages over `log` crate
//!
//! * **extensible** - `slog` crate provides core functionality: very basic
//!   and portable standard feature-set based on open `trait`s. This allows
//!   implementing new features that can be independently published.
//! * **composable** - `trait`s that `slog` exposes to provide extensibility
//!   are designed to be easy to efficiently reuse and combine. By combining
//!   different functionalities every application can specify when, where and
//!   how exactly process logging data from the application and it's
//!   dependencies.
//! * **flexible** - `slog` does not constrain logging to just one globally
//!   registered backend. Parts of your application can handle logging
//!   in a customized way, or completely independently.
//! * **structured** and both **human and machine readable** - By keeping the
//!   key-value data format and retaining its type information, meaning of logging
//!   data is preserved.  Data can be serialized to machine readable formats like
//!   JSON and send it to data-mining system for further analysis etc. On the
//!   other hand, when presenting on screen, logging information can be presented
//!   in aesthetically pleasing and easy to understand way.
//! * **contextual** - `slog`'s `Logger` objects carry set of key-value data
//!   pairs that contains the context of logging - information that otherwise
//!   would have to be repeated in every logging statement.
//!
//! ## `slog` features
//!
//! * performance oriented; read [what makes slog
//!   fast](https://github.com/slog-rs/slog/wiki/What-makes-slog-fast) and see:
//!   [slog bench log](https://github.com/dpc/slog-rs/wiki/Bench-log)
//!   * lazily evaluation through closure values
//!   * async IO support included: see [`slog-async`
//!     crate](https://docs.rs/slog-async)
//! * `#![no_std]` support (with opt-out `std` cargo feature flag)
//! * support for named format arguments (eg. `info!(logger, "printed {line_count} lines", line_count = 2);`)
//!   for easy bridging the human readable and machine-readable output
//! * tree-structured loggers
//! * modular, lightweight and very extensible
//!   * tiny core crate that does not pull any dependencies
//!   * feature-crates for specific functionality
//!   * using `slog` in library does not force users of the library to use slog
//!     (but provides additional functionality); see [example how to use
//!     `slog` in library](https://github.com/slog-rs/example-lib)
//! * backward and forward compatibility with `log` crate:
//!   see [`slog-stdlog` crate](https://docs.rs/slog-stdlog)
//! * convenience crates:
//!   * logging-scopes for implicit `Logger` passing: see
//!     [slog-scope crate](https://docs.rs/slog-scope)
//! * many existing core&community provided features:
//!   * multiple outputs
//!   * filtering control
//!       * compile-time log level filter using cargo features (same as in `log`
//!         crate)
//!       * by level, msg, and any other meta-data
//!       * [`slog-envlogger`](https://github.com/slog-rs/envlogger) - port of
//!         `env_logger`
//!       * terminal output, with color support: see [`slog-term`
//!         crate](https://docs.rs/slog-term)
//!  * [json](https://docs.rs/slog-json)
//!      * [bunyan](https://docs.rs/slog-bunyan)
//!  * [syslog](https://docs.rs/slog-syslog)
//!    and [journald](https://docs.rs/slog-journald) support
//!  * run-time configuration:
//!      * run-time behavior change;
//!        see [slog-atomic](https://docs.rs/slog-atomic)
//!      * run-time configuration; see
//!        [slog-config crate](https://docs.rs/slog-config)
//!
//!
//! [env_logger]: https://crates.io/crates/env_logger
//!
//! ## Notable details
//!
//! **Note:** At compile time `slog` by default removes trace and debug level
//! statements in release builds, and trace level records in debug builds. This
//! makes `trace` and `debug` level logging records practically free, which
//! should encourage using them freely. If you want to enable trace/debug
//! messages or raise the compile time logging level limit, use the following in
//! your `Cargo.toml`:
//!
//! ```norust
//! slog = { version = ... ,
//!          features = ["max_level_trace", "release_max_level_warn"] }
//! ```
//!
//! Root drain (passed to `Logger::root`) must be one that does not ever return
//! errors. This forces user to pick error handing strategy.
//! `Drain::fuse()` or `Drain::ignore_res()`.
//!
//! [env_logger]: https://crates.io/crates/env_logger
//! [fn-overview]: https://github.com/dpc/slog-rs/wiki/Functional-overview
//! [atomic-switch]: https://docs.rs/slog-atomic/
//!
//! ## Where to start
//!
//! [`Drain`](trait.Drain.html), [`Logger`](struct.Logger.html) and
//! [`log` macro](macro.log.html) are the most important elements of
//! slog. Make sure to read their respective documentation
//!
//! Typically the biggest problem is creating a `Drain`
//!
//!
//! ### Logging to the terminal
//!
//! ```ignore
//! #[macro_use]
//! extern crate slog;
//! extern crate slog_term;
//! extern crate slog_async;
//!
//! use slog::Drain;
//!
//! fn main() {
//!     let decorator = slog_term::TermDecorator::new().build();
//!     let drain = slog_term::FullFormat::new(decorator).build().fuse();
//!     let drain = slog_async::Async::new(drain).build().fuse();
//!
//!     let _log = slog::Logger::root(drain, o!());
//! }
//! ```
//!
//! ### Logging to a file
//!
//! ```ignore
//! #[macro_use]
//! extern crate slog;
//! extern crate slog_term;
//! extern crate slog_async;
//!
//! use std::fs::OpenOptions;
//! use slog::Drain;
//!
//! fn main() {
//!    let log_path = "target/your_log_file_path.log";
//!    let file = OpenOptions::new()
//!       .create(true)
//!       .write(true)
//!       .truncate(true)
//!       .open(log_path)
//!       .unwrap();
//!
//!     let decorator = slog_term::PlainDecorator::new(file);
//!     let drain = slog_term::FullFormat::new(decorator).build().fuse();
//!     let drain = slog_async::Async::new(drain).build().fuse();
//!
//!     let _log = slog::Logger::root(drain, o!());
//! }
//! ```
//!
//! You can consider using `slog-json` instead of `slog-term`.
//! `slog-term` only coincidently fits the role of a file output format. A
//! proper `slog-file` crate with suitable format, log file rotation and other
//! file-logging related features would be awesome. Contributions are welcome!
//!
//! ### Change logging level at runtime
//!
//! ```ignore
//! #[macro_use]
//! extern crate slog;
//! extern crate slog_term;
//! extern crate slog_async;
//!
//! use slog::Drain;
//!
//! use std::sync::{Arc, atomic};
//! use std::sync::atomic::Ordering;
//! use std::result;
//!
//! /// Custom Drain logic
//! struct RuntimeLevelFilter<D>{
//!    drain: D,
//!    on: Arc<atomic::AtomicBool>,
//! }
//!
//! impl<D> Drain for RuntimeLevelFilter<D>
//!     where D : Drain {
//!     type Ok = Option<D::Ok>;
//!     type Err = Option<D::Err>;
//!
//!     fn log(&self,
//!           record: &slog::Record,
//!           values: &slog::OwnedKVList)
//!           -> result::Result<Self::Ok, Self::Err> {
//!           let current_level = if self.on.load(Ordering::Relaxed) {
//!               slog::Level::Trace
//!           } else {
//!               slog::Level::Info
//!           };
//!
//!           if record.level().is_at_least(current_level) {
//!               self.drain.log(
//!                   record,
//!                   values
//!               )
//!               .map(Some)
//!               .map_err(Some)
//!           } else {
//!               Ok(None)
//!           }
//!       }
//!   }
//!
//! fn main() {
//!     // atomic variable controlling logging level
//!     let on = Arc::new(atomic::AtomicBool::new(false));
//!
//!     let decorator = slog_term::TermDecorator::new().build();
//!     let drain = slog_term::FullFormat::new(decorator).build();
//!     let drain = RuntimeLevelFilter {
//!         drain: drain,
//!         on: on.clone(),
//!     }.fuse();
//!     let drain = slog_async::Async::new(drain).build().fuse();
//!
//!     let _log = slog::Logger::root(drain, o!());
//!
//!     // switch level in your code
//!     on.store(true, Ordering::Relaxed);
//! }
//! ```
//!
//! Why is this not an existing crate? Because there are multiple ways to
//! achieve the same result, and each application might come with it's own
//! variation. Supporting a more general solution is a maintenance effort.
//! There is also nothing stopping anyone from publishing their own crate
//! implementing it.
//!
//! Alternative to the above aproach is `slog-atomic` crate. It implements
//! swapping whole parts of `Drain` logging hierarchy.
//!
//! ## Examples & help
//!
//! Basic examples that are kept up-to-date are typically stored in
//! respective git repository, under `examples/` subdirectory. Eg.
//! [slog-term examples](https://github.com/slog-rs/term/tree/master/examples).
//!
//! [slog-rs wiki pages](https://github.com/slog-rs/slog/wiki) contain
//! some pages about `slog-rs` technical details.
//!
//! Source code of other [software using
//! slog-rs](https://crates.io/crates/slog/reverse_dependencies) can
//! be an useful reference.
//!
//! Visit [slog-rs gitter channel](https://gitter.im/slog-rs/slog) for immediate
//! help.
//!
//! ## Migrating from slog v1 to slog v2
//!
//! ### Key-value pairs come now after format string
//!
//! ```
//! #[macro_use]
//! extern crate slog;
//!
//! fn main() {
//!     let drain = slog::Discard;
//!     let root = slog::Logger::root(drain, o!());
//!     info!(root, "formatted: {}", 1; "log-key" => true);
//! }
//! ```
//!
//! See more information about format at [`log`](macro.log.html).
//!
//! ### `slog-streamer` is gone
//!
//! Create simple terminal logger like this:
//!
//! ```ignore
//! #[macro_use]
//! extern crate slog;
//! extern crate slog_term;
//! extern crate slog_async;
//!
//! use slog::Drain;
//!
//! fn main() {
//!     let decorator = slog_term::TermDecorator::new().build();
//!     let drain = slog_term::FullFormat::new(decorator).build().fuse();
//!     let drain = slog_async::Async::new(drain).build().fuse();
//!
//!     let _log = slog::Logger::root(drain, o!());
//! }
//! ```
//!
//!
//! ### Logging macros now takes ownership of values.
//!
//! Pass them by reference: `&x`.
//!
// }}}

// {{{ Imports & meta
#![warn(missing_docs)]

mod key {
    #[cfg(feature = "dynamic-keys")]
    mod dynamic {
        use std::borrow::Cow;
        use std::cmp::PartialEq;
        use std::convert::{AsRef, From};
        use std::fmt;
        use std::hash::{Hash, Hasher};
        use std::iter::{FromIterator, IntoIterator};
        use std::string::String;
        use std::string::ToString;

        /// Opaque Key is a representation of a key.
        ///
        /// It is owned, and largely forms a contract for
        /// key to follow.
        #[derive(Clone)]
        pub struct Key {
            data: Cow<'static, str>,
        }

        impl Key {
            /// Returns the length of self.
            pub fn len(&self) -> usize {
                self.data.len()
            }

            /// Returns the length of self.
            pub fn is_empty(&self) -> bool {
                self.data.is_empty()
            }

            /// Take as a `str`
            pub fn as_str(&self) -> &str {
                self.data.as_ref()
            }

            /// Take as a reference
            pub fn as_ref(&self) -> &str {
                self.as_str()
            }
        }

        impl Default for Key {
            fn default() -> Key {
                Key {
                    data: Cow::Borrowed(""),
                }
            }
        }

        impl Hash for Key {
            fn hash<H: Hasher>(&self, state: &mut H) {
                match self.data {
                    Cow::Borrowed(ref ptr) => ptr.hash(state),
                    Cow::Owned(ref ptr) => ptr.hash(state),
                }
            }
        }

        impl From<&'static str> for Key {
            #[inline(always)]
            fn from(data: &'static str) -> Key {
                Key {
                    data: Cow::Borrowed(data),
                }
            }
        }

        impl From<String> for Key {
            fn from(data: String) -> Key {
                Key {
                    data: Cow::Owned(data),
                }
            }
        }

        impl From<Key> for String {
            fn from(s: Key) -> String {
                s.to_string()
            }
        }

        impl FromIterator<char> for Key {
            fn from_iter<I: IntoIterator<Item = char>>(iter: I) -> Key {
                Key {
                    data: Cow::Owned(iter.into_iter().collect::<String>()),
                }
            }
        }

        impl<'a> FromIterator<&'a char> for Key {
            fn from_iter<I: IntoIterator<Item = &'a char>>(iter: I) -> Key {
                Key {
                    data: Cow::Owned(iter.into_iter().collect::<String>()),
                }
            }
        }

        impl FromIterator<String> for Key {
            fn from_iter<I: IntoIterator<Item = String>>(iter: I) -> Key {
                Key {
                    data: Cow::Owned(iter.into_iter().collect::<String>()),
                }
            }
        }

        impl<'a> FromIterator<&'a String> for Key {
            fn from_iter<I: IntoIterator<Item = &'a String>>(iter: I) -> Key {
                Key {
                    data: Cow::Owned(iter.into_iter().map(|x| x.as_str()).collect::<String>()),
                }
            }
        }

        impl<'a> FromIterator<&'a str> for Key {
            fn from_iter<I: IntoIterator<Item = &'a str>>(iter: I) -> Key {
                Key {
                    data: Cow::Owned(iter.into_iter().collect::<String>()),
                }
            }
        }

        impl<'a> FromIterator<Cow<'a, str>> for Key {
            fn from_iter<I: IntoIterator<Item = Cow<'a, str>>>(iter: I) -> Key {
                Key {
                    data: Cow::Owned(iter.into_iter().collect::<String>()),
                }
            }
        }

        impl PartialEq<str> for Key {
            fn eq(&self, other: &str) -> bool {
                self.as_ref().eq(other)
            }
        }

        impl PartialEq<String> for Key {
            fn eq(&self, other: &String) -> bool {
                self.as_ref().eq(other.as_str())
            }
        }

        impl PartialEq<Self> for Key {
            fn eq(&self, other: &Self) -> bool {
                self.as_ref().eq(other.as_ref())
            }
        }

        impl AsRef<str> for Key {
            fn as_ref<'a>(&'a self) -> &'a str {
                self.data.as_ref()
            }
        }

        impl fmt::Display for Key {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                match self.data {
                    Cow::Borrowed(ref ptr) => write!(f, "{}", ptr),
                    Cow::Owned(ref ptr) => write!(f, "{}", ptr),
                }
            }
        }

        impl fmt::Debug for Key {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                match self.data {
                    Cow::Borrowed(ref ptr) => write!(f, "{:?}", ptr),
                    Cow::Owned(ref ptr) => write!(f, "{:?}", ptr),
                }
            }
        }
    }

    #[cfg(feature = "dynamic-keys")]
    pub use self::dynamic::Key;

    #[cfg(not(feature = "dynamic-keys"))]
    #[path = "static.rs"]
    mod static_ {
        /// Key type
        pub type Key = &'static str;
    }

    #[cfg(not(feature = "dynamic-keys"))]
    pub use self::static_::Key;
}

pub use self::key::Key;

use core::str::FromStr;
use core::{convert, fmt, result};
use std::boxed::Box;
use std::panic::{RefUnwindSafe, UnwindSafe};
use std::rc::Rc;
use std::string::String;
use std::sync::Arc;
// }}}

// {{{ Macros
/// Macro for building group of key-value pairs:
/// [`OwnedKV`](struct.OwnedKV.html)
///
/// ```
/// #[macro_use]
/// extern crate slog;
///
/// fn main() {
///     let drain = slog::Discard;
///     let _root = slog::Logger::root(
///         drain,
///         o!("key1" => "value1", "key2" => "value2")
///     );
/// }
/// ```
#[macro_export(local_inner_macros)]
macro_rules! o(
    ($($args:tt)*) => {
        $crate::slog::OwnedKV(kv!($($args)*))
    };
);

/// Macro for building group of key-value pairs (alias)
///
/// Use in case of macro name collisions
#[macro_export(local_inner_macros)]
macro_rules! slog_o(
    ($($args:tt)*) => {
        $crate::slog::OwnedKV(slog_kv!($($args)*))
    };
);

/// Macro for building group of key-value pairs in
/// [`BorrowedKV`](struct.BorrowedKV.html)
///
/// In most circumstances using this macro directly is unnecessary and `info!`
/// and other wrappers over `log!` should be used instead.
#[macro_export(local_inner_macros)]
macro_rules! b(
    ($($args:tt)*) => {
        $crate::slog::BorrowedKV(&kv!($($args)*))
    };
);

/// Alias of `b`
#[macro_export(local_inner_macros)]
macro_rules! slog_b(
    ($($args:tt)*) => {
        $crate::slog::BorrowedKV(&slog_kv!($($args)*))
    };
);

/// Macro for build `KV` implementing type
///
/// You probably want to use `o!` or `b!` instead.
#[macro_export(local_inner_macros)]
macro_rules! kv(
    (@ $args_ready:expr; $k:expr => %$v:expr) => {
        kv!(@ ($crate::slog::SingleKV::from(($k, __slog_builtin!(@format_args "{}", $v))), $args_ready); )
    };
    (@ $args_ready:expr; $k:expr => %$v:expr, $($args:tt)* ) => {
        kv!(@ ($crate::slog::SingleKV::from(($k, __slog_builtin!(@format_args "{}", $v))), $args_ready); $($args)* )
    };
    (@ $args_ready:expr; $k:expr => ?$v:expr) => {
        kv!(@ ($crate::slog::SingleKV::from(($k, __slog_builtin!(@format_args "{:?}", $v))), $args_ready); )
    };
    (@ $args_ready:expr; $k:expr => ?$v:expr, $($args:tt)* ) => {
        kv!(@ ($crate::slog::SingleKV::from(($k, __slog_builtin!(@format_args "{:?}", $v))), $args_ready); $($args)* )
    };
    (@ $args_ready:expr; $k:expr => $v:expr) => {
        kv!(@ ($crate::slog::SingleKV::from(($k, $v)), $args_ready); )
    };
    (@ $args_ready:expr; $k:expr => $v:expr, $($args:tt)* ) => {
        kv!(@ ($crate::slog::SingleKV::from(($k, $v)), $args_ready); $($args)* )
    };
    (@ $args_ready:expr; $kv:expr) => {
        kv!(@ ($kv, $args_ready); )
    };
    (@ $args_ready:expr; $kv:expr, $($args:tt)* ) => {
        kv!(@ ($kv, $args_ready); $($args)* )
    };
    (@ $args_ready:expr; ) => {
        $args_ready
    };
    (@ $args_ready:expr;, ) => {
        $args_ready
    };
    ($($args:tt)*) => {
        kv!(@ (); $($args)*)
    };
);

/// Alias of `kv`
#[macro_export(local_inner_macros)]
macro_rules! slog_kv(
    (@ $args_ready:expr; $k:expr => %$v:expr) => {
        slog_kv!(@ ($crate::slog::SingleKV::from(($k, __slog_builtin!(@format_args "{}", $v))), $args_ready); )
    };
    (@ $args_ready:expr; $k:expr => %$v:expr, $($args:tt)* ) => {
        slog_kv!(@ ($crate::slog::SingleKV::from(($k, __slog_builtin!(@format_args "{}", $v))), $args_ready); $($args)* )
    };
    (@ $args_ready:expr; $k:expr => ?$v:expr) => {
        kv!(@ ($crate::slog::SingleKV::from(($k, __slog_builtin!(@format_args "{:?}", $v))), $args_ready); )
    };
    (@ $args_ready:expr; $k:expr => ?$v:expr, $($args:tt)* ) => {
        kv!(@ ($crate::slog::SingleKV::from(($k, __slog_builtin!(@format_args "{:?}", $v))), $args_ready); $($args)* )
    };
    (@ $args_ready:expr; $k:expr => $v:expr) => {
        slog_kv!(@ ($crate::slog::SingleKV::from(($k, $v)), $args_ready); )
    };
    (@ $args_ready:expr; $k:expr => $v:expr, $($args:tt)* ) => {
        slog_kv!(@ ($crate::slog::SingleKV::from(($k, $v)), $args_ready); $($args)* )
    };
    (@ $args_ready:expr; $slog_kv:expr) => {
        slog_kv!(@ ($slog_kv, $args_ready); )
    };
    (@ $args_ready:expr; $slog_kv:expr, $($args:tt)* ) => {
        slog_kv!(@ ($slog_kv, $args_ready); $($args)* )
    };
    (@ $args_ready:expr; ) => {
        $args_ready
    };
    (@ $args_ready:expr;, ) => {
        $args_ready
    };
    ($($args:tt)*) => {
        slog_kv!(@ (); $($args)*)
    };
);

#[macro_export(local_inner_macros)]
/// Create `RecordStatic` at the given code location
macro_rules! record_static(
    ($lvl:expr, $tag:expr,) => { record_static!($lvl, $tag) };
    ($lvl:expr, $tag:expr) => {{
        static LOC : $crate::slog::RecordLocation = $crate::slog::RecordLocation {
            file: __slog_builtin!(@file),
            line: __slog_builtin!(@line),
            column: __slog_builtin!(@column),
            function: "",
            module: __slog_builtin!(@module_path),
        };
        $crate::slog::RecordStatic {
            location : &LOC,
            level: $lvl,
            tag : $tag,
        }
    }};
);

#[macro_export(local_inner_macros)]
/// Create `RecordStatic` at the given code location (alias)
macro_rules! slog_record_static(
    ($lvl:expr, $tag:expr,) => { slog_record_static!($lvl, $tag) };
    ($lvl:expr, $tag:expr) => {{
        static LOC : $crate::slog::RecordLocation = $crate::slog::RecordLocation {
            file: __slog_builtin!(@file),
            line: __slog_builtin!(@line),
            column: __slog_builtin!(@column),
            function: "",
            module: __slog_builtin!(@module_path),
        };
        $crate::slog::RecordStatic {
            location : &LOC,
            level: $lvl,
            tag: $tag,
        }
    }};
);

#[macro_export(local_inner_macros)]
/// Create `Record` at the given code location
///
/// Note that this requires that `lvl` and `tag` are compile-time constants. If
/// you need them to *not* be compile-time, such as when recreating a `Record`
/// from a serialized version, use `Record::new` instead.
macro_rules! record(
    ($lvl:expr, $tag:expr, $args:expr, $b:expr,) => {
        record!($lvl, $tag, $args, $b)
    };
    ($lvl:expr, $tag:expr, $args:expr, $b:expr) => {{
        #[allow(dead_code)]
        static RS : $crate::slog::RecordStatic<'static> = record_static!($lvl, $tag);
        $crate::slog::Record::new(&RS, $args, $b)
    }};
);

#[macro_export(local_inner_macros)]
/// Create `Record` at the given code location (alias)
macro_rules! slog_record(
    ($lvl:expr, $tag:expr, $args:expr, $b:expr,) => {
        slog_record!($lvl, $tag, $args, $b)
    };
    ($lvl:expr, $tag:expr, $args:expr, $b:expr) => {{
        static RS : $crate::slog::RecordStatic<'static> = slog_record_static!($lvl,
                                                                        $tag);
        $crate::slog::Record::new(&RS, $args, $b)
    }};
);

/// Log message a logging record
///
/// Use wrappers `error!`, `warn!` etc. instead
///
/// The `max_level_*` and `release_max_level*` cargo features can be used to
/// statically disable logging at various levels. See [slog notable
/// details](index.html#notable-details)
///
/// Use [version with longer name](macro.slog_log.html) if you want to prevent
/// clash with legacy `log` crate macro names.
///
/// ## Supported invocations
///
/// ### Simple
///
/// ```
/// #[macro_use]
/// extern crate slog;
///
/// fn main() {
///     let drain = slog::Discard;
///     let root = slog::Logger::root(
///         drain,
///         o!("key1" => "value1", "key2" => "value2")
///     );
///     info!(root, "test info log"; "log-key" => true);
/// }
/// ```
///
/// Note that `"key" => value` part is optional:
///
/// ```
/// #[macro_use]
/// extern crate slog;
///
/// fn main() {
///     let drain = slog::Discard;
///     let root = slog::Logger::root(
///         drain, o!("key1" => "value1", "key2" => "value2")
///     );
///     info!(root, "test info log");
/// }
/// ```
///
/// ### Formatting support:
///
/// ```
/// #[macro_use]
/// extern crate slog;
///
/// fn main() {
///     let drain = slog::Discard;
///     let root = slog::Logger::root(drain,
///         o!("key1" => "value1", "key2" => "value2")
///     );
///     info!(root, "formatted {num_entries} entries of {}", "something", num_entries = 2; "log-key" => true);
/// }
/// ```
///
/// Note:
///
/// * `;` is used to separate message arguments and key value pairs.
/// * message behaves like `format!`/`format_args!`
/// * Named arguments to messages will be added to key-value pairs as well!
///
/// `"key" => value` part is optional:
///
/// ```
/// #[macro_use]
/// extern crate slog;
///
/// fn main() {
///     let drain = slog::Discard;
///     let root = slog::Logger::root(
///         drain, o!("key1" => "value1", "key2" => "value2")
///     );
///     info!(root, "formatted: {}", 1);
/// }
/// ```
///
/// Use formatting support wisely. Prefer named arguments, so the associated
/// data is not "lost" by becoming an untyped string in the message.
///
/// ### Tags
///
/// All above versions can be supplemented with a tag - string literal prefixed
/// with `#`.
///
/// ```
/// #[macro_use]
/// extern crate slog;
///
/// fn main() {
///     let drain = slog::Discard;
///     let root = slog::Logger::root(drain,
///         o!("key1" => "value1", "key2" => "value2")
///     );
///     let ops = 3;
///     info!(
///         root,
///         #"performance-metric", "thread speed"; "ops_per_sec" => ops
///     );
/// }
/// ```
///
/// See `Record::tag()` for more information about tags.
///
/// ### Own implementations of `KV` and `Value`
///
/// List of key value pairs is a comma separated list of key-values. Typically,
/// a designed syntax is used in form of `k => v` where `k` can be any type
/// that implements `Value` type.
///
/// It's possible to directly specify type that implements `KV` trait without
/// `=>` syntax.
///
/// ```
/// #[macro_use]
/// extern crate slog;
///
/// use slog::*;
///
/// fn main() {
///     struct MyKV;
///     struct MyV;
///
///     impl KV for MyKV {
///        fn serialize(&self,
///                     _record: &Record,
///                     serializer: &mut Serializer)
///                    -> Result {
///            serializer.emit_u32("MyK", 16)
///        }
///     }
///
///     impl Value for MyV {
///        fn serialize(&self,
///                     _record: &Record,
///                     key : Key,
///                     serializer: &mut Serializer)
///                    -> Result {
///            serializer.emit_u32("MyKV", 16)
///        }
///     }
///
///     let drain = slog::Discard;
///
///     let root = slog::Logger::root(drain, o!(MyKV));
///
///     info!(
///         root,
///         "testing MyV"; "MyV" => MyV
///     );
/// }
/// ```
///
/// ### `fmt::Display` and `fmt::Debug` values
///
/// Value of any type that implements `std::fmt::Display` can be prefixed with
/// `%` in `k => v` expression to use it's text representation returned by
/// `format_args!("{}", v)`. This is especially useful for errors. Not that
/// this does not allocate any `String` since it operates on `fmt::Arguments`.
///
/// Similarly to use `std::fmt::Debug` value can be prefixed with `?`.
///
/// ```
/// #[macro_use]
/// extern crate slog;
/// use std::fmt::Write;
///
/// fn main() {
///     let drain = slog::Discard;
///     let log  = slog::Logger::root(drain, o!());
///
///     let mut output = String::new();
///
///     if let Err(e) = write!(&mut output, "write to string") {
///         error!(log, "write failed"; "err" => %e);
///     }
/// }
/// ```
#[macro_export(local_inner_macros)]
macro_rules! slog_log(
    // `2` means that `;` was already found
   (2 @ { $($fmt:tt)* }, { $($kv:tt)* },  $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr) => {
      $l.log(&slog_record!($lvl, $tag, &__slog_builtin!(@format_args $msg_fmt, $($fmt)*), slog_b!($($kv)*)))
   };
   (2 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr,) => {
       slog_log!(2 @ { $($fmt)* }, { $($kv)* }, $l, $lvl, $tag, $msg_fmt)
   };
   (2 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr;) => {
       slog_log!(2 @ { $($fmt)* }, { $($kv)* }, $l, $lvl, $tag, $msg_fmt)
   };
   (2 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr, $($args:tt)*) => {
       slog_log!(2 @ { $($fmt)* }, { $($kv)* $($args)*}, $l, $lvl, $tag, $msg_fmt)
   };
    // `1` means that we are still looking for `;`
    // -- handle named arguments to format string
   (1 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr, $k:ident = $v:expr) => {
       slog_log!(2 @ { $($fmt)* $k = $v }, { $($kv)* __slog_builtin!(@stringify $k) => $v, }, $l, $lvl, $tag, $msg_fmt)
   };
   (1 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr, $k:ident = $v:expr;) => {
       slog_log!(2 @ { $($fmt)* $k = $v }, { $($kv)* __slog_builtin!(@stringify $k) => $v, }, $l, $lvl, $tag, $msg_fmt)
   };
   (1 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr, $k:ident = $v:expr,) => {
       slog_log!(2 @ { $($fmt)* $k = $v }, { $($kv)* __slog_builtin!(@stringify $k) => $v, }, $l, $lvl, $tag, $msg_fmt)
   };
   (1 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr, $k:ident = $v:expr; $($args:tt)*) => {
       slog_log!(2 @ { $($fmt)* $k = $v }, { $($kv)* __slog_builtin!(@stringify $k) => $v, }, $l, $lvl, $tag, $msg_fmt, $($args)*)
   };
   (1 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr, $k:ident = $v:expr, $($args:tt)*) => {
       slog_log!(1 @ { $($fmt)* $k = $v, }, { $($kv)* __slog_builtin!(@stringify $k) => $v, }, $l, $lvl, $tag, $msg_fmt, $($args)*)
   };
    // -- look for `;` termination
   (1 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr,) => {
       slog_log!(2 @ { $($fmt)* }, { $($kv)* }, $l, $lvl, $tag, $msg_fmt)
   };
   (1 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr) => {
       slog_log!(2 @ { $($fmt)* }, { $($kv)* }, $l, $lvl, $tag, $msg_fmt)
   };
   (1 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr, ; $($args:tt)*) => {
       slog_log!(1 @ { $($fmt)* }, { $($kv)* }, $l, $lvl, $tag, $msg_fmt; $($args)*)
   };
   (1 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr; $($args:tt)*) => {
       slog_log!(2 @ { $($fmt)* }, { $($kv)* }, $l, $lvl, $tag, $msg_fmt, $($args)*)
   };
    // -- must be normal argument to format string
   (1 @ { $($fmt:tt)* }, { $($kv:tt)* }, $l:expr, $lvl:expr, $tag:expr, $msg_fmt:expr, $f:tt $($args:tt)*) => {
       slog_log!(1 @ { $($fmt)* $f }, { $($kv)* }, $l, $lvl, $tag, $msg_fmt, $($args)*)
   };
   ($l:expr, $lvl:expr, $tag:expr, $($args:tt)*) => {
       if $lvl.as_usize() <= $crate::slog::__slog_static_max_level().as_usize() {
           slog_log!(1 @ { }, { }, $l, $lvl, $tag, $($args)*)
       }
   };
);
/// Log critical level record
///
/// See `log` for documentation.
#[macro_export(local_inner_macros)]
macro_rules! slog_crit(
    ($l:expr, #$tag:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Critical, $tag, $($args)+)
    };
    ($l:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Critical, "", $($args)+)
    };
);

/// Log error level record
///
/// See `log` for documentation.
#[macro_export(local_inner_macros)]
macro_rules! slog_error(
    ($l:expr, #$tag:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Error, $tag, $($args)+)
    };
    ($l:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Error, "", $($args)+)
    };
);

/// Log warning level record
///
/// See `log` for documentation.
#[macro_export(local_inner_macros)]
macro_rules! slog_warn(
    ($l:expr, #$tag:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Warning, $tag, $($args)+)
    };
    ($l:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Warning, "", $($args)+)
    };
);

/// Log info level record
///
/// See `slog_log` for documentation.
#[macro_export(local_inner_macros)]
macro_rules! slog_info(
    ($l:expr, #$tag:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Info, $tag, $($args)+)
    };
    ($l:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Info, "", $($args)+)
    };
);

/// Log debug level record
///
/// See `log` for documentation.
#[macro_export(local_inner_macros)]
macro_rules! slog_debug(
    ($l:expr, #$tag:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Debug, $tag, $($args)+)
    };
    ($l:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Debug, "", $($args)+)
    };
);

/// Log trace level record
///
/// See `log` for documentation.
#[macro_export(local_inner_macros)]
macro_rules! slog_trace(
    ($l:expr, #$tag:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Trace, $tag, $($args)+)
    };
    ($l:expr, $($args:tt)+) => {
        slog_log!($l, $crate::slog::Level::Trace, "", $($args)+)
    };
    ($($args:tt)+) => {
        slog_log!($crate::slog::Level::Trace, $($args)+)
    };
);

/// Helper macro for using the built-in macros inside of
/// exposed macros with `local_inner_macros` attribute.
#[doc(hidden)]
#[macro_export]
macro_rules! __slog_builtin {
    (@format_args $($t:tt)*) => ( format_args!($($t)*) );
    (@stringify $($t:tt)*) => ( stringify!($($t)*) );
    (@file) => ( file!() );
    (@line) => ( line!() );
    (@column) => ( column!() );
    (@module_path) => ( module_path!() );
}

// }}}

// {{{ Logger
/// Logging handle used to execute logging statements
///
/// In an essence `Logger` instance holds two pieces of information:
///
/// * drain - destination where to forward logging `Record`s for
/// processing.
/// * context - list of key-value pairs associated with it.
///
/// Root `Logger` is created with a `Drain` that will be cloned to every
/// member of it's hierarchy.
///
/// Child `Logger` are built from existing ones, and inherit their key-value
/// pairs, which can be supplemented with additional ones.
///
/// Cloning existing loggers and creating new ones is cheap. Loggers can be
/// freely passed around the code and between threads.
///
/// `Logger`s are `Sync+Send` - there's no need to synchronize accesses to them,
/// as they can accept logging records from multiple threads at once. They can
/// be sent to any thread. Because of that they require the `Drain` to be
/// `Sync+Sync` as well. Not all `Drain`s are `Sync` or `Send` but they can
/// often be made so by wrapping in a `Mutex` and/or `Arc`.
///
/// `Logger` implements `Drain` trait. Any logging `Record` delivered to
/// a `Logger` functioning as a `Drain`, will be delivered to it's `Drain`
/// with existing key-value pairs appended to the `Logger`s key-value pairs.
/// By itself it's effectively very similar to `Logger` being an ancestor
/// of `Logger` that originated the logging `Record`. Combined with other
/// `Drain`s, allows custom processing logic for a sub-tree of a whole logging
/// tree.
///
/// Logger is parametrized over type of a `Drain` associated with it (`D`). It
/// default to type-erased version so `Logger` without any type annotation
/// means `Logger<Arc<SendSyncRefUnwindSafeDrain<Ok = (), Err = Never>>>`. See
/// `Logger::root_typed` and `Logger::to_erased` for more information.
#[derive(Clone)]
pub struct Logger<D = Arc<SendSyncRefUnwindSafeDrain<Ok = (), Err = Never>>>
where
    D: SendSyncUnwindSafeDrain<Ok = (), Err = Never>,
{
    drain: D,
    list: OwnedKVList,
}

impl<D> Logger<D>
where
    D: SendSyncUnwindSafeDrain<Ok = (), Err = Never>,
{
    /// Build a root `Logger`
    ///
    /// Root logger starts a new tree associated with a given `Drain`. Root
    /// logger drain must return no errors. See `Drain::ignore_res()` and
    /// `Drain::fuse()`.
    ///
    /// All children and their children (and so on), form one logging tree
    /// sharing a common drain. See `Logger::new`.
    ///
    /// This version (as opposed to `Logger:root_typed`) will take `drain` and
    /// made it into `Arc<SendSyncRefUnwindSafeDrain<Ok = (), Err = Never>>`.
    /// This is typically the most convenient way to work with `Logger`s.
    ///
    /// Use `o!` macro to build `OwnedKV` object.
    ///
    /// ```
    /// #[macro_use]
    /// extern crate slog;
    ///
    /// fn main() {
    ///     let _root = slog::Logger::root(
    ///         slog::Discard,
    ///         o!("key1" => "value1", "key2" => "value2"),
    ///     );
    /// }
    /// ```
    pub fn root<T>(drain: D, values: OwnedKV<T>) -> Logger
    where
        D: 'static + SendSyncRefUnwindSafeDrain<Err = Never, Ok = ()>,
        T: SendSyncRefUnwindSafeKV + 'static,
    {
        Logger {
            drain: Arc::new(drain) as Arc<SendSyncRefUnwindSafeDrain<Ok = (), Err = Never>>,
            list: OwnedKVList::root(values),
        }
    }

    /// Build a root `Logger` that retains `drain` type
    ///
    /// Unlike `Logger::root`, this constructor retains the type of a `drain`,
    /// which allows highest performance possible by eliminating indirect call
    /// on `Drain::log`, and allowing monomorphization of `Logger` and `Drain`
    /// objects.
    ///
    /// If you don't understand the implications, you should probably just
    /// ignore it.
    ///
    /// See `Logger:into_erased` and `Logger::to_erased` for conversion from
    /// type returned by this function to version that would be returned by
    /// `Logger::root`.
    pub fn root_typed<T>(drain: D, values: OwnedKV<T>) -> Logger<D>
    where
        D: 'static + SendSyncUnwindSafeDrain<Err = Never, Ok = ()> + Sized,
        T: SendSyncRefUnwindSafeKV + 'static,
    {
        Logger {
            drain,
            list: OwnedKVList::root(values),
        }
    }

    /// Build a child logger
    ///
    /// Child logger inherits all existing key-value pairs from its parent and
    /// supplements them with additional ones.
    ///
    /// Use `o!` macro to build `OwnedKV` object.
    ///
    /// ### Drain cloning (`D : Clone` requirement)
    ///
    /// All children, their children and so on, form one tree sharing a
    /// common drain. This drain, will be `Clone`d when this method is called.
    /// That is why `Clone` must be implemented for `D` in `Logger<D>::new`.
    ///
    /// For some `Drain` types `Clone` is cheap or even free (a no-op). This is
    /// the case for any `Logger` returned by `Logger::root` and it's children.
    ///
    /// When using `Logger::root_typed`, it's possible that cloning might be
    /// expensive, or even impossible.
    ///
    /// The reason why wrapping in an `Arc` is not done internally, and exposed
    /// to the user is performance. Calling `Drain::log` through an `Arc` is
    /// tiny bit slower than doing it directly.
    ///
    /// ```
    /// #[macro_use]
    /// extern crate slog;
    ///
    /// fn main() {
    ///     let root = slog::Logger::root(slog::Discard,
    ///         o!("key1" => "value1", "key2" => "value2"));
    ///     let _log = root.new(o!("key" => "value"));
    /// }
    #[allow(clippy::wrong_self_convention)]
    pub fn new<T>(&self, values: OwnedKV<T>) -> Logger<D>
    where
        T: SendSyncRefUnwindSafeKV + 'static,
        D: Clone,
    {
        Logger {
            drain: self.drain.clone(),
            list: OwnedKVList::new(values, self.list.node.clone()),
        }
    }

    /// Log one logging `Record`
    ///
    /// Use specific logging functions instead. See `log!` macro
    /// documentation.
    #[inline]
    pub fn log(&self, record: &Record) {
        let _ = self.drain.log(record, &self.list);
    }

    /// Get list of key-value pairs assigned to this `Logger`
    pub fn list(&self) -> &OwnedKVList {
        &self.list
    }

    /// Convert to default, "erased" type:
    /// `Logger<Arc<SendSyncUnwindSafeDrain>>`
    ///
    /// Useful to adapt `Logger<D : Clone>` to an interface expecting
    /// `Logger<Arc<...>>`.
    ///
    /// Note that calling on a `Logger<Arc<...>>` will convert it to
    /// `Logger<Arc<Arc<...>>>` which is not optimal. This might be fixed when
    /// Rust gains trait implementation specialization.
    pub fn into_erased(self) -> Logger<Arc<SendSyncRefUnwindSafeDrain<Ok = (), Err = Never>>>
    where
        D: SendRefUnwindSafeDrain + 'static,
    {
        Logger {
            drain: Arc::new(self.drain) as Arc<SendSyncRefUnwindSafeDrain<Ok = (), Err = Never>>,
            list: self.list,
        }
    }

    /// Create a copy with "erased" type
    ///
    /// See `into_erased`
    pub fn to_erased(&self) -> Logger<Arc<SendSyncRefUnwindSafeDrain<Ok = (), Err = Never>>>
    where
        D: SendRefUnwindSafeDrain + 'static + Clone,
    {
        self.clone().into_erased()
    }
}

impl<D> fmt::Debug for Logger<D>
where
    D: SendSyncUnwindSafeDrain<Ok = (), Err = Never>,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Logger{:?}", self.list)?;
        Ok(())
    }
}

impl<D> Drain for Logger<D>
where
    D: SendSyncUnwindSafeDrain<Ok = (), Err = Never>,
{
    type Ok = ();
    type Err = Never;

    fn log(&self, record: &Record, values: &OwnedKVList) -> result::Result<Self::Ok, Self::Err> {
        let chained = OwnedKVList {
            node: Arc::new(MultiListNode {
                next_node: values.node.clone(),
                node: self.list.node.clone(),
            }),
        };
        self.drain.log(record, &chained)
    }

    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        self.drain.is_enabled(level)
    }
}

// {{{ Drain
/// Logging drain
///
/// `Drain`s typically mean destination for logs, but `slog` generalizes the
/// term.
///
/// `Drain`s are responsible for handling logging statements (`Record`s) from
/// `Logger`s associated with them: filtering, modifying, formatting
/// and writing the log records into given destination(s).
///
/// It's a typical pattern to parametrize `Drain`s over `Drain` traits to allow
/// composing `Drain`s.
///
/// Implementing this trait allows writing custom `Drain`s. Slog users should
/// not be afraid of implementing their own `Drain`s. Any custom log handling
/// logic should be implemented as a `Drain`.
pub trait Drain {
    /// Type returned by this drain
    ///
    /// It can be useful in some circumstances, but rarely. It will probably
    /// default to `()` once https://github.com/rust-lang/rust/issues/29661 is
    /// stable.
    type Ok;
    /// Type of potential errors that can be returned by this `Drain`
    type Err;
    /// Handle one logging statement (`Record`)
    ///
    /// Every logging `Record` built from a logging statement (eg.
    /// `info!(...)`), and key-value lists of a `Logger` it was executed on
    /// will be passed to the root drain registered during `Logger::root`.
    ///
    /// Typically `Drain`s:
    ///
    /// * pass this information (or not) to the sub-logger(s) (filters)
    /// * format and write the information the a destination (writers)
    /// * deal with the errors returned from the sub-logger(s)
    fn log(&self, record: &Record, values: &OwnedKVList) -> result::Result<Self::Ok, Self::Err>;

    /// **Avoid**: Check if messages at the specified log level are **maybe**
    /// enabled for this logger.
    ///
    /// The purpose of it so to allow **imprecise** detection if a given logging
    /// level has any chance of actually being logged. This might be used
    /// to explicitly skip needless computation.
    ///
    /// **It is best effort, can return false positives, but not false negatives.**
    ///
    /// The logger is still free to ignore records even if the level is enabled,
    /// so an enabled level doesn't necessarily guarantee that the record will
    /// actually be logged.
    ///
    /// This function is somewhat needless, and is better expressed by using
    /// lazy values (see `FnValue`).  A `FnValue` is more precise and does not
    /// require additional (potentially recursive) calls to do something that
    /// `log` will already do anyways (making decision if something should be
    /// logged or not).
    ///
    /// ```
    /// # #[macro_use]
    /// # extern crate slog;
    /// # use slog::*;
    /// # fn main() {
    /// let logger = Logger::root(Discard, o!());
    /// if logger.is_enabled(Level::Debug) {
    ///     let num = 5.0f64;
    ///     let sqrt = num.sqrt();
    ///     debug!(logger, "Sqrt"; "num" => num, "sqrt" => sqrt);
    /// }
    /// # }
    /// ```
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        level.as_usize() <= __slog_static_max_level().as_usize()
    }

    /// **Avoid**: See `is_enabled`
    #[inline]
    fn is_critical_enabled(&self) -> bool {
        self.is_enabled(Level::Critical)
    }

    /// **Avoid**: See `is_enabled`
    #[inline]
    fn is_error_enabled(&self) -> bool {
        self.is_enabled(Level::Error)
    }

    /// **Avoid**: See `is_enabled`
    #[inline]
    fn is_warning_enabled(&self) -> bool {
        self.is_enabled(Level::Warning)
    }

    /// **Avoid**: See `is_enabled`
    #[inline]
    fn is_info_enabled(&self) -> bool {
        self.is_enabled(Level::Info)
    }

    /// **Avoid**: See `is_enabled`
    #[inline]
    fn is_debug_enabled(&self) -> bool {
        self.is_enabled(Level::Debug)
    }

    /// **Avoid**: See `is_enabled`
    #[inline]
    fn is_trace_enabled(&self) -> bool {
        self.is_enabled(Level::Trace)
    }

    /// Pass `Drain` through a closure, eg. to wrap
    /// into another `Drain`.
    ///
    /// ```
    /// #[macro_use]
    /// extern crate slog;
    /// use slog::*;
    ///
    /// fn main() {
    ///     let _drain = Discard.map(Fuse);
    /// }
    /// ```
    fn map<F, R>(self, f: F) -> R
    where
        Self: Sized,
        F: FnOnce(Self) -> R,
    {
        f(self)
    }

    /// Filter logging records passed to `Drain`
    ///
    /// Wrap `Self` in `Filter`
    ///
    /// This will convert `self` to a `Drain that ignores `Record`s
    /// for which `f` returns false.
    fn filter<F>(self, f: F) -> Filter<Self, F>
    where
        Self: Sized,
        F: FilterFn,
    {
        Filter::new(self, f)
    }

    /// Filter logging records passed to `Drain` (by level)
    ///
    /// Wrap `Self` in `LevelFilter`
    ///
    /// This will convert `self` to a `Drain that ignores `Record`s of
    /// logging lever smaller than `level`.
    fn filter_level(self, level: Level) -> LevelFilter<Self>
    where
        Self: Sized,
    {
        LevelFilter(self, level)
    }

    /// Map logging errors returned by this drain
    ///
    /// `f` is a closure that takes `Drain::Err` returned by a given
    /// drain, and returns new error of potentially different type
    fn map_err<F, E>(self, f: F) -> MapError<Self, E>
    where
        Self: Sized,
        F: MapErrFn<Self::Err, E>,
    {
        MapError::new(self, f)
    }

    /// Ignore results returned by this drain
    ///
    /// Wrap `Self` in `IgnoreResult`
    fn ignore_res(self) -> IgnoreResult<Self>
    where
        Self: Sized,
    {
        IgnoreResult::new(self)
    }

    /// Make `Self` panic when returning any errors
    ///
    /// Wrap `Self` in `Map`
    fn fuse(self) -> Fuse<Self>
    where
        Self::Err: fmt::Debug,
        Self: Sized,
    {
        self.map(Fuse)
    }
}

impl<'a, D: Drain + 'a> Drain for &'a D {
    type Ok = D::Ok;
    type Err = D::Err;
    #[inline]
    fn log(&self, record: &Record, values: &OwnedKVList) -> result::Result<Self::Ok, Self::Err> {
        (**self).log(record, values)
    }
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        (**self).is_enabled(level)
    }
}

impl<'a, D: Drain + 'a> Drain for &'a mut D {
    type Ok = D::Ok;
    type Err = D::Err;
    #[inline]
    fn log(&self, record: &Record, values: &OwnedKVList) -> result::Result<Self::Ok, Self::Err> {
        (**self).log(record, values)
    }
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        (**self).is_enabled(level)
    }
}

/// `Send + Sync + UnwindSafe` bound
///
/// This type is used to enforce `Drain`s associated with `Logger`s
/// are thread-safe.
pub trait SendSyncUnwindSafe: Send + Sync + UnwindSafe {}

impl<T> SendSyncUnwindSafe for T where T: Send + Sync + UnwindSafe + ?Sized {}

/// `Drain + Send + Sync + UnwindSafe` bound
///
/// This type is used to enforce `Drain`s associated with `Logger`s
/// are thread-safe.
pub trait SendSyncUnwindSafeDrain: Drain + Send + Sync + UnwindSafe {}

impl<T> SendSyncUnwindSafeDrain for T where T: Drain + Send + Sync + UnwindSafe + ?Sized {}

/// `Drain + Send + Sync + RefUnwindSafe` bound
///
/// This type is used to enforce `Drain`s associated with `Logger`s
/// are thread-safe.
pub trait SendSyncRefUnwindSafeDrain: Drain + Send + Sync + RefUnwindSafe {}

impl<T> SendSyncRefUnwindSafeDrain for T where T: Drain + Send + Sync + RefUnwindSafe + ?Sized {}

/// Function that can be used in `MapErr` drain
pub trait MapErrFn<EI, EO>:
    'static + Sync + Send + UnwindSafe + RefUnwindSafe + Fn(EI) -> EO
{
}

impl<T, EI, EO> MapErrFn<EI, EO> for T where
    T: 'static + Sync + Send + ?Sized + UnwindSafe + RefUnwindSafe + Fn(EI) -> EO
{
}

/// Function that can be used in `Filter` drain
pub trait FilterFn:
    'static + Sync + Send + UnwindSafe + RefUnwindSafe + Fn(&Record) -> bool
{
}

impl<T> FilterFn for T where
    T: 'static + Sync + Send + ?Sized + UnwindSafe + RefUnwindSafe + Fn(&Record) -> bool
{
}

/// `Drain + Send + RefUnwindSafe` bound
pub trait SendRefUnwindSafeDrain: Drain + Send + RefUnwindSafe {}

impl<T> SendRefUnwindSafeDrain for T where T: Drain + Send + RefUnwindSafe + ?Sized {}

impl<D: Drain + ?Sized> Drain for Box<D> {
    type Ok = D::Ok;
    type Err = D::Err;
    fn log(&self, record: &Record, o: &OwnedKVList) -> result::Result<Self::Ok, D::Err> {
        (**self).log(record, o)
    }
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        (**self).is_enabled(level)
    }
}

impl<D: Drain + ?Sized> Drain for Arc<D> {
    type Ok = D::Ok;
    type Err = D::Err;
    fn log(&self, record: &Record, o: &OwnedKVList) -> result::Result<Self::Ok, D::Err> {
        (**self).log(record, o)
    }
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        (**self).is_enabled(level)
    }
}

/// `Drain` discarding everything
///
/// `/dev/null` of `Drain`s
#[derive(Debug, Copy, Clone)]
pub struct Discard;

impl Drain for Discard {
    type Ok = ();
    type Err = Never;
    fn log(&self, _: &Record, _: &OwnedKVList) -> result::Result<(), Never> {
        Ok(())
    }
    #[inline]
    fn is_enabled(&self, _1: Level) -> bool {
        false
    }
}

/// `Drain` filtering records
///
/// Wraps another `Drain` and passes `Record`s to it, only if they satisfy a
/// given condition.
#[derive(Debug, Clone)]
pub struct Filter<D: Drain, F>(pub D, pub F)
where
    F: Fn(&Record) -> bool + 'static + Send + Sync;

impl<D: Drain, F> Filter<D, F>
where
    F: FilterFn,
{
    /// Create `Filter` wrapping given `drain`
    pub fn new(drain: D, cond: F) -> Self {
        Filter(drain, cond)
    }
}

impl<D: Drain, F> Drain for Filter<D, F>
where
    F: FilterFn,
{
    type Ok = Option<D::Ok>;
    type Err = D::Err;
    fn log(
        &self,
        record: &Record,
        logger_values: &OwnedKVList,
    ) -> result::Result<Self::Ok, Self::Err> {
        if (self.1)(record) {
            Ok(Some(self.0.log(record, logger_values)?))
        } else {
            Ok(None)
        }
    }
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        /*
         * This is one of the reasons we can't guarantee the value is actually logged.
         * The filter function is given dynamic control over whether or not the record is logged
         * and could filter stuff out even if the log level is supposed to be enabled
         */
        self.0.is_enabled(level)
    }
}

/// `Drain` filtering records by `Record` logging level
///
/// Wraps a drain and passes records to it, only
/// if their level is at least given level.
///
/// TODO: Remove this type. This drain is a special case of `Filter`, but
/// because `Filter` can not use static dispatch ATM due to Rust limitations
/// that will be lifted in the future, it is a standalone type.
/// Reference: https://github.com/rust-lang/rust/issues/34511
#[derive(Debug, Clone)]
pub struct LevelFilter<D: Drain>(pub D, pub Level);

impl<D: Drain> LevelFilter<D> {
    /// Create `LevelFilter`
    pub fn new(drain: D, level: Level) -> Self {
        LevelFilter(drain, level)
    }
}

impl<D: Drain> Drain for LevelFilter<D> {
    type Ok = Option<D::Ok>;
    type Err = D::Err;
    fn log(
        &self,
        record: &Record,
        logger_values: &OwnedKVList,
    ) -> result::Result<Self::Ok, Self::Err> {
        if record.level().is_at_least(self.1) {
            Ok(Some(self.0.log(record, logger_values)?))
        } else {
            Ok(None)
        }
    }
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        level.is_at_least(self.1) && self.0.is_enabled(level)
    }
}

/// `Drain` mapping error returned by another `Drain`
///
/// See `Drain::map_err` for convenience function.
pub struct MapError<D: Drain, E> {
    drain: D,
    // eliminated dynamic dispatch, after rust learns `-> impl Trait`
    map_fn: Box<MapErrFn<D::Err, E, Output = E>>,
}

impl<D: Drain, E> MapError<D, E> {
    /// Create `Filter` wrapping given `drain`
    pub fn new<F>(drain: D, map_fn: F) -> Self
    where
        F: MapErrFn<<D as Drain>::Err, E>,
    {
        MapError {
            drain,
            map_fn: Box::new(map_fn),
        }
    }
}

impl<D: Drain, E> Drain for MapError<D, E> {
    type Ok = D::Ok;
    type Err = E;

    #[allow(clippy::redundant_closure)]
    fn log(
        &self,
        record: &Record,
        logger_values: &OwnedKVList,
    ) -> result::Result<Self::Ok, Self::Err> {
        self.drain
            .log(record, logger_values)
            .map_err(|e| (self.map_fn)(e))
    }
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        self.drain.is_enabled(level)
    }
}

/// `Drain` duplicating records into two other `Drain`s
///
/// Can be nested for more than two outputs.
#[derive(Debug, Clone)]
pub struct Duplicate<D1: Drain, D2: Drain>(pub D1, pub D2);

impl<D1: Drain, D2: Drain> Duplicate<D1, D2> {
    /// Create `Duplicate`
    pub fn new(drain1: D1, drain2: D2) -> Self {
        Duplicate(drain1, drain2)
    }
}

impl<D1: Drain, D2: Drain> Drain for Duplicate<D1, D2> {
    type Ok = (D1::Ok, D2::Ok);
    type Err = (
        result::Result<D1::Ok, D1::Err>,
        result::Result<D2::Ok, D2::Err>,
    );
    fn log(
        &self,
        record: &Record,
        logger_values: &OwnedKVList,
    ) -> result::Result<Self::Ok, Self::Err> {
        let res1 = self.0.log(record, logger_values);
        let res2 = self.1.log(record, logger_values);

        match (res1, res2) {
            (Ok(o1), Ok(o2)) => Ok((o1, o2)),
            (r1, r2) => Err((r1, r2)),
        }
    }
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        self.0.is_enabled(level) || self.1.is_enabled(level)
    }
}

/// `Drain` panicking on error
///
/// `Logger` requires a root drain to handle all errors (`Drain::Error == ()`),
/// `Fuse` will wrap a `Drain` and panic if it returns any errors.
///
/// Note: `Drain::Err` must implement `Display` (for displaying on panick). It's
/// easy to create own `Fuse` drain if this requirement can't be fulfilled.
#[derive(Debug, Clone)]
pub struct Fuse<D: Drain>(pub D)
where
    D::Err: fmt::Debug;

impl<D: Drain> Fuse<D>
where
    D::Err: fmt::Debug,
{
    /// Create `Fuse` wrapping given `drain`
    pub fn new(drain: D) -> Self {
        Fuse(drain)
    }
}

impl<D: Drain> Drain for Fuse<D>
where
    D::Err: fmt::Debug,
{
    type Ok = ();
    type Err = Never;
    fn log(&self, record: &Record, logger_values: &OwnedKVList) -> result::Result<Self::Ok, Never> {
        let _ = self
            .0
            .log(record, logger_values)
            .unwrap_or_else(|e| panic!("slog::Fuse Drain: {:?}", e));
        Ok(())
    }
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        self.0.is_enabled(level)
    }
}

/// `Drain` ignoring result
///
/// `Logger` requires a root drain to handle all errors (`Drain::Err=()`), and
/// returns nothing (`Drain::Ok=()`) `IgnoreResult` will ignore any result
/// returned by the `Drain` it wraps.
#[derive(Clone)]
pub struct IgnoreResult<D: Drain> {
    drain: D,
}

impl<D: Drain> IgnoreResult<D> {
    /// Create `IgnoreResult` wrapping `drain`
    pub fn new(drain: D) -> Self {
        IgnoreResult { drain }
    }
}

impl<D: Drain> Drain for IgnoreResult<D> {
    type Ok = ();
    type Err = Never;
    fn log(&self, record: &Record, logger_values: &OwnedKVList) -> result::Result<(), Never> {
        let _ = self.drain.log(record, logger_values);
        Ok(())
    }

    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        self.drain.is_enabled(level)
    }
}

/// Error returned by `Mutex<D : Drain>`
#[derive(Clone)]
pub enum MutexDrainError<D: Drain> {
    /// Error acquiring mutex
    Mutex,
    /// Error returned by drain
    Drain(D::Err),
}

impl<D> fmt::Debug for MutexDrainError<D>
where
    D: Drain,
    D::Err: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> result::Result<(), fmt::Error> {
        match *self {
            MutexDrainError::Mutex => write!(f, "MutexDrainError::Mutex"),
            MutexDrainError::Drain(ref e) => e.fmt(f),
        }
    }
}

impl<D> std::error::Error for MutexDrainError<D>
where
    D: Drain,
    D::Err: fmt::Debug + fmt::Display + std::error::Error,
{
    fn description(&self) -> &str {
        match *self {
            MutexDrainError::Mutex => "Mutex acquire failed",
            MutexDrainError::Drain(ref e) => e.description(),
        }
    }

    fn cause(&self) -> Option<&std::error::Error> {
        match *self {
            MutexDrainError::Mutex => None,
            MutexDrainError::Drain(ref e) => Some(e),
        }
    }
}

impl<'a, D: Drain> From<std::sync::PoisonError<std::sync::MutexGuard<'a, D>>>
    for MutexDrainError<D>
{
    fn from(_: std::sync::PoisonError<std::sync::MutexGuard<'a, D>>) -> MutexDrainError<D> {
        MutexDrainError::Mutex
    }
}

impl<D: Drain> fmt::Display for MutexDrainError<D>
where
    D::Err: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> result::Result<(), fmt::Error> {
        match *self {
            MutexDrainError::Mutex => write!(f, "MutexError"),
            MutexDrainError::Drain(ref e) => write!(f, "{}", e),
        }
    }
}

impl<D: Drain> Drain for std::sync::Mutex<D> {
    type Ok = D::Ok;
    type Err = MutexDrainError<D>;
    fn log(
        &self,
        record: &Record,
        logger_values: &OwnedKVList,
    ) -> result::Result<Self::Ok, Self::Err> {
        let d = self.lock()?;
        d.log(record, logger_values).map_err(MutexDrainError::Drain)
    }
    #[inline]
    fn is_enabled(&self, level: Level) -> bool {
        self.lock().ok().map_or(true, |lock| lock.is_enabled(level))
    }
}
// }}}

// {{{ Level & FilterLevel
/// Official capitalized logging (and logging filtering) level names
///
/// In order of `as_usize()`.
pub static LOG_LEVEL_NAMES: [&'static str; 7] =
    ["OFF", "CRITICAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE"];

/// Official capitalized logging (and logging filtering) short level names
///
/// In order of `as_usize()`.
pub static LOG_LEVEL_SHORT_NAMES: [&'static str; 7] =
    ["OFF", "CRIT", "ERRO", "WARN", "INFO", "DEBG", "TRCE"];

/// Logging level associated with a logging `Record`
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum Level {
    /// Critical
    Critical,
    /// Error
    Error,
    /// Warning
    Warning,
    /// Info
    Info,
    /// Debug
    Debug,
    /// Trace
    Trace,
}

/// Logging filtering level
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum FilterLevel {
    /// Log nothing
    Off,
    /// Log critical level only
    Critical,
    /// Log only error level and above
    Error,
    /// Log only warning level and above
    Warning,
    /// Log only info level and above
    Info,
    /// Log only debug level and above
    Debug,
    /// Log everything
    Trace,
}

impl Level {
    /// Convert to `str` from `LOG_LEVEL_SHORT_NAMES`
    pub fn as_short_str(&self) -> &'static str {
        LOG_LEVEL_SHORT_NAMES[self.as_usize()]
    }

    /// Convert to `str` from `LOG_LEVEL_NAMES`
    pub fn as_str(&self) -> &'static str {
        LOG_LEVEL_NAMES[self.as_usize()]
    }

    /// Cast `Level` to ordering integer
    ///
    /// `Critical` is the smallest and `Trace` the biggest value
    #[inline]
    pub fn as_usize(&self) -> usize {
        match *self {
            Level::Critical => 1,
            Level::Error => 2,
            Level::Warning => 3,
            Level::Info => 4,
            Level::Debug => 5,
            Level::Trace => 6,
        }
    }

    /// Get a `Level` from an `usize`
    ///
    /// This complements `as_usize`
    #[inline]
    pub fn from_usize(u: usize) -> Option<Level> {
        match u {
            1 => Some(Level::Critical),
            2 => Some(Level::Error),
            3 => Some(Level::Warning),
            4 => Some(Level::Info),
            5 => Some(Level::Debug),
            6 => Some(Level::Trace),
            _ => None,
        }
    }
}

impl FilterLevel {
    /// Convert to `usize` value
    ///
    /// `Off` is 0, and `Trace` 6
    #[inline]
    pub fn as_usize(&self) -> usize {
        match *self {
            FilterLevel::Off => 0,
            FilterLevel::Critical => 1,
            FilterLevel::Error => 2,
            FilterLevel::Warning => 3,
            FilterLevel::Info => 4,
            FilterLevel::Debug => 5,
            FilterLevel::Trace => 6,
        }
    }

    /// Get a `FilterLevel` from an `usize`
    ///
    /// This complements `as_usize`
    #[inline]
    pub fn from_usize(u: usize) -> Option<FilterLevel> {
        match u {
            0 => Some(FilterLevel::Off),
            1 => Some(FilterLevel::Critical),
            2 => Some(FilterLevel::Error),
            3 => Some(FilterLevel::Warning),
            4 => Some(FilterLevel::Info),
            5 => Some(FilterLevel::Debug),
            6 => Some(FilterLevel::Trace),
            _ => None,
        }
    }

    /// Maximum logging level (log everything)
    #[inline]
    pub fn max() -> Self {
        FilterLevel::Trace
    }

    /// Minimum logging level (log nothing)
    #[inline]
    pub fn min() -> Self {
        FilterLevel::Off
    }
}

#[rustfmt::skip]
static ASCII_LOWERCASE_MAP: [u8; 256] =
    [0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b,
     0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
     0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, b' ', b'!', b'"', b'#',
     b'$', b'%', b'&', b'\'', b'(', b')', b'*', b'+', b',', b'-', b'.', b'/',
     b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', b':', b';',
     b'<', b'=', b'>', b'?', b'@', b'a', b'b', b'c', b'd', b'e', b'f', b'g',
     b'h', b'i', b'j', b'k', b'l', b'm', b'n', b'o', b'p', b'q', b'r', b's',
     b't', b'u', b'v', b'w', b'x', b'y', b'z', b'[', b'\\', b']', b'^', b'_',
     b'`', b'a', b'b', b'c', b'd', b'e', b'f', b'g', b'h', b'i', b'j', b'k',
     b'l', b'm', b'n', b'o', b'p', b'q', b'r', b's', b't', b'u', b'v', b'w',
     b'x', b'y', b'z', b'{', b'|', b'}', b'~', 0x7f, 0x80, 0x81, 0x82, 0x83,
     0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f,
     0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b,
     0x9c, 0x9d, 0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7,
     0xa8, 0xa9, 0xaa, 0xab, 0xac, 0xad, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3,
     0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf,
     0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xcb,
     0xcc, 0xcd, 0xce, 0xcf, 0xd0, 0xd1, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7,
     0xd8, 0xd9, 0xda, 0xdb, 0xdc, 0xdd, 0xde, 0xdf, 0xe0, 0xe1, 0xe2, 0xe3,
     0xe4, 0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xeb, 0xec, 0xed, 0xee, 0xef,
     0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xfb,
     0xfc, 0xfd, 0xfe, 0xff];

impl FromStr for Level {
    type Err = ();
    fn from_str(level: &str) -> core::result::Result<Level, ()> {
        LOG_LEVEL_NAMES
            .iter()
            .position(|&name| {
                name.as_bytes()
                    .iter()
                    .zip(level.as_bytes().iter())
                    .all(|(a, b)| {
                        ASCII_LOWERCASE_MAP[*a as usize] == ASCII_LOWERCASE_MAP[*b as usize]
                    })
            })
            .map(|p| Level::from_usize(p).unwrap())
            .ok_or(())
    }
}

impl FromStr for FilterLevel {
    type Err = ();
    fn from_str(level: &str) -> core::result::Result<FilterLevel, ()> {
        LOG_LEVEL_NAMES
            .iter()
            .position(|&name| {
                name.as_bytes()
                    .iter()
                    .zip(level.as_bytes().iter())
                    .all(|(a, b)| {
                        ASCII_LOWERCASE_MAP[*a as usize] == ASCII_LOWERCASE_MAP[*b as usize]
                    })
            })
            .map(|p| FilterLevel::from_usize(p).unwrap())
            .ok_or(())
    }
}

impl fmt::Display for Level {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_short_str())
    }
}

impl Level {
    /// Returns true if `self` is at least `level` logging level
    #[inline]
    pub fn is_at_least(&self, level: Self) -> bool {
        self.as_usize() <= level.as_usize()
    }
}

#[test]
fn level_at_least() {
    assert!(Level::Debug.is_at_least(Level::Debug));
    assert!(Level::Debug.is_at_least(Level::Trace));
    assert!(!Level::Debug.is_at_least(Level::Info));
}

#[test]
fn filterlevel_sanity() {
    assert!(Level::Critical.as_usize() > FilterLevel::Off.as_usize());
    assert!(Level::Critical.as_usize() == FilterLevel::Critical.as_usize());
    assert!(Level::Trace.as_usize() == FilterLevel::Trace.as_usize());
}

#[test]
fn level_from_str() {
    assert_eq!("info".parse::<FilterLevel>().unwrap(), FilterLevel::Info);
}
// }}}

// {{{ Record
#[doc(hidden)]
#[derive(Clone, Copy)]
pub struct RecordLocation<'a> {
    /// File
    pub file: &'a str,
    /// Line
    pub line: u32,
    /// Column (currently not implemented)
    pub column: u32,
    /// Function (currently not implemented)
    pub function: &'a str,
    /// Module
    pub module: &'a str,
}
/// Information that can be static in the given record thus allowing to optimize
/// record creation to be done mostly at compile-time.
///
/// This should be constructed via the `record_static!` macro.
pub struct RecordStatic<'a> {
    /// Code location
    #[doc(hidden)]
    pub location: &'a RecordLocation<'a>,
    /// Tag
    #[doc(hidden)]
    pub tag: &'a str,
    /// Logging level
    #[doc(hidden)]
    pub level: Level,
}

/// One logging record
///
/// Corresponds to one logging statement like `info!(...)` and carries all it's
/// data: eg. message, immediate key-value pairs and key-value pairs of `Logger`
/// used to execute it.
///
/// Record is passed to a `Logger`, which delivers it to it's own `Drain`,
/// where actual logging processing is implemented.
pub struct Record<'a> {
    rstatic: &'a RecordStatic<'a>,
    msg: &'a fmt::Arguments<'a>,
    kv: BorrowedKV<'a>,
}

impl<'a> Record<'a> {
    /// Create a new `Record`
    ///
    /// Most of the time, it is slightly more performant to construct a `Record`
    /// via the `record!` macro because it enforces that the *entire*
    /// `RecordStatic` is built at compile-time.
    ///
    /// Use this if runtime record creation is a requirement, as is the case with
    /// [slog-async](https://docs.rs/slog-async/latest/slog_async/struct.Async.html),
    /// for example.
    #[inline]
    pub fn new(s: &'a RecordStatic<'a>, msg: &'a fmt::Arguments<'a>, kv: BorrowedKV<'a>) -> Self {
        Record {
            rstatic: s,
            msg,
            kv,
        }
    }

    /// Get a log record message
    pub fn msg(&self) -> &fmt::Arguments {
        self.msg
    }

    /// Get record logging level
    pub fn level(&self) -> Level {
        self.rstatic.level
    }

    /// Get line number
    pub fn line(&self) -> u32 {
        self.rstatic.location.line
    }

    /// Get line number
    pub fn location(&self) -> &RecordLocation {
        self.rstatic.location
    }

    /// Get error column
    pub fn column(&self) -> u32 {
        self.rstatic.location.column
    }

    /// Get file path
    pub fn file(&self) -> &'a str {
        self.rstatic.location.file
    }

    /// Get tag
    ///
    /// Tag is information that can be attached to `Record` that is not meant
    /// to be part of the norma key-value pairs, but only as an ad-hoc control
    /// flag for quick lookup in the `Drain`s. As such should be used carefully
    /// and mostly in application code (as opposed to libraries) - where tag
    /// meaning across the system can be coordinated. When used in libraries,
    /// make sure to prefix is with something reasonably distinct, like create
    /// name.
    pub fn tag(&self) -> &str {
        self.rstatic.tag
    }

    /// Get module
    pub fn module(&self) -> &'a str {
        self.rstatic.location.module
    }

    /// Get function (placeholder)
    ///
    /// There's currently no way to obtain that information
    /// in Rust at compile time, so it is not implemented.
    ///
    /// It will be implemented at first opportunity, and
    /// it will not be considered a breaking change.
    pub fn function(&self) -> &'a str {
        self.rstatic.location.function
    }

    /// Get key-value pairs
    pub fn kv(&self) -> BorrowedKV {
        BorrowedKV(self.kv.0)
    }
}
// }}}

// {{{ Serializer

#[cfg(macro_workaround)]
macro_rules! impl_default_as_fmt{
    (#[$m:meta] $($t:tt)+) => {
        #[$m]
        impl_default_as_fmt!($($t)*);
    };
    ($t:ty => $f:ident) => {
        #[allow(missing_docs)]
        fn $f(&mut self, key : Key, val : $t)
            -> Result {
                self.emit_arguments(key, &format_args!("{}", val))
            }
    };
}

#[cfg(not(macro_workaround))]
macro_rules! impl_default_as_fmt{
    ($(#[$m:meta])* $t:ty => $f:ident) => {
        $(#[$m])*
        fn $f(&mut self, key : Key, val : $t)
            -> Result {
                self.emit_arguments(key, &format_args!("{}", val))
            }
    };
}

/// Serializer
///
/// Drains using `Format` will internally use
/// types implementing this trait.
pub trait Serializer {
    impl_default_as_fmt! {
        /// Emit `usize`
        usize => emit_usize
    }
    impl_default_as_fmt! {
        /// Emit `isize`
        isize => emit_isize
    }
    impl_default_as_fmt! {
        /// Emit `bool`
        bool => emit_bool
    }
    impl_default_as_fmt! {
        /// Emit `char`
        char => emit_char
    }
    impl_default_as_fmt! {
        /// Emit `u8`
        u8 => emit_u8
    }
    impl_default_as_fmt! {
        /// Emit `i8`
        i8 => emit_i8
    }
    impl_default_as_fmt! {
        /// Emit `u16`
        u16 => emit_u16
    }
    impl_default_as_fmt! {
        /// Emit `i16`
        i16 => emit_i16
    }
    impl_default_as_fmt! {
        /// Emit `u32`
        u32 => emit_u32
    }
    impl_default_as_fmt! {
        /// Emit `i32`
        i32 => emit_i32
    }
    impl_default_as_fmt! {
        /// Emit `f32`
        f32 => emit_f32
    }
    impl_default_as_fmt! {
        /// Emit `u64`
        u64 => emit_u64
    }
    impl_default_as_fmt! {
        /// Emit `i64`
        i64 => emit_i64
    }
    impl_default_as_fmt! {
        /// Emit `f64`
        f64 => emit_f64
    }
    impl_default_as_fmt! {
        /// Emit `u128`
        #[cfg(integer128)]
        u128 => emit_u128
    }
    impl_default_as_fmt! {
        /// Emit `i128`
        #[cfg(integer128)]
        i128 => emit_i128
    }
    impl_default_as_fmt! {
        /// Emit `&str`
        &str => emit_str
    }

    /// Emit `()`
    fn emit_unit(&mut self, key: Key) -> Result {
        self.emit_arguments(key, &format_args!("()"))
    }

    /// Emit `None`
    fn emit_none(&mut self, key: Key) -> Result {
        self.emit_arguments(key, &format_args!(""))
    }

    /// Emit `fmt::Arguments`
    ///
    /// This is the only method that has to implemented, but for performance and
    /// to retain type information most serious `Serializer`s will want to
    /// implement all other methods as well.
    fn emit_arguments(&mut self, key: Key, val: &fmt::Arguments) -> Result;
}

/// Serializer to closure adapter.
///
/// Formats all arguments as `fmt::Arguments` and passes them to a given closure.
struct AsFmtSerializer<F>(pub F)
where
    F: for<'a> FnMut(Key, fmt::Arguments<'a>) -> Result;

impl<F> Serializer for AsFmtSerializer<F>
where
    F: for<'a> FnMut(Key, fmt::Arguments<'a>) -> Result,
{
    fn emit_arguments(&mut self, key: Key, val: &fmt::Arguments) -> Result {
        (self.0)(key, *val)
    }
}
// }}}

// }}}

// {{{ Value
/// Value that can be serialized
///
/// Types that implement this type implement custome serialization in the
/// structured part of the log macros. Without an implementation of `Value` for
/// your type you must emit using either the `?` "debug", `%` "display" or
/// [`SerdeValue`](trait.SerdeValue.html) (if you have the `nested-values`
/// feature enabled) formatters.
///
/// # Example
///
/// ```
/// use slog::{Key, Value, Record, Result, Serializer};
/// struct MyNewType(i64);
///
/// impl Value for MyNewType {
///     fn serialize(&self, _rec: &Record, key: Key, serializer: &mut Serializer) -> Result {
///         serializer.emit_i64(key, self.0)
///     }
/// }
/// ```
///
/// See also [`KV`](trait.KV.html) for formatting both the key and value.
pub trait Value {
    /// Serialize self into `Serializer`
    ///
    /// Structs implementing this trait should generally
    /// only call respective methods of `serializer`.
    fn serialize(&self, record: &Record, key: Key, serializer: &mut Serializer) -> Result;
}

impl<'a, V> Value for &'a V
where
    V: Value + ?Sized,
{
    fn serialize(&self, record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        (*self).serialize(record, key, serializer)
    }
}

macro_rules! impl_value_for {
    ($t:ty, $f:ident) => {
        impl Value for $t {
            fn serialize(&self, _record: &Record, key: Key, serializer: &mut Serializer) -> Result {
                serializer.$f(key, *self)
            }
        }
    };
}

impl_value_for!(usize, emit_usize);
impl_value_for!(isize, emit_isize);
impl_value_for!(bool, emit_bool);
impl_value_for!(char, emit_char);
impl_value_for!(u8, emit_u8);
impl_value_for!(i8, emit_i8);
impl_value_for!(u16, emit_u16);
impl_value_for!(i16, emit_i16);
impl_value_for!(u32, emit_u32);
impl_value_for!(i32, emit_i32);
impl_value_for!(f32, emit_f32);
impl_value_for!(u64, emit_u64);
impl_value_for!(i64, emit_i64);
impl_value_for!(f64, emit_f64);
#[cfg(integer128)]
impl_value_for!(u128, emit_u128);
#[cfg(integer128)]
impl_value_for!(i128, emit_i128);

impl Value for () {
    fn serialize(&self, _record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        serializer.emit_unit(key)
    }
}

impl Value for str {
    fn serialize(&self, _record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        serializer.emit_str(key, self)
    }
}

impl<'a> Value for fmt::Arguments<'a> {
    fn serialize(&self, _record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        serializer.emit_arguments(key, self)
    }
}

impl Value for String {
    fn serialize(&self, _record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        serializer.emit_str(key, self.as_str())
    }
}

impl<T: Value> Value for Option<T> {
    fn serialize(&self, record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        match *self {
            Some(ref s) => s.serialize(record, key, serializer),
            None => serializer.emit_none(key),
        }
    }
}

impl<T> Value for Box<T>
where
    T: Value + ?Sized,
{
    fn serialize(&self, record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        (**self).serialize(record, key, serializer)
    }
}
impl<T> Value for Arc<T>
where
    T: Value + ?Sized,
{
    fn serialize(&self, record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        (**self).serialize(record, key, serializer)
    }
}

impl<T> Value for Rc<T>
where
    T: Value,
{
    fn serialize(&self, record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        (**self).serialize(record, key, serializer)
    }
}

impl<T> Value for core::num::Wrapping<T>
where
    T: Value,
{
    fn serialize(&self, record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        self.0.serialize(record, key, serializer)
    }
}

impl<'a> Value for std::path::Display<'a> {
    fn serialize(&self, _record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        serializer.emit_arguments(key, &format_args!("{}", *self))
    }
}

impl Value for std::net::SocketAddr {
    fn serialize(&self, _record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        serializer.emit_arguments(key, &format_args!("{}", self))
    }
}

/// Explicit lazy-closure `Value`
pub struct FnValue<V: Value, F>(pub F)
where
    F: for<'c, 'd> Fn(&'c Record<'d>) -> V;

impl<'a, V: 'a + Value, F> Value for FnValue<V, F>
where
    F: 'a + for<'c, 'd> Fn(&'c Record<'d>) -> V,
{
    fn serialize(&self, record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        (self.0)(record).serialize(record, key, serializer)
    }
}

#[deprecated(note = "Renamed to `PushFnValueSerializer`")]
/// Old name of `PushFnValueSerializer`
pub type PushFnSerializer<'a> = PushFnValueSerializer<'a>;

/// Handle passed to `PushFnValue` closure
///
/// It makes sure only one value is serialized, and will automatically emit
/// `()` if nothing else was serialized.
pub struct PushFnValueSerializer<'a> {
    record: &'a Record<'a>,
    key: Key,
    serializer: &'a mut Serializer,
    done: bool,
}

impl<'a> PushFnValueSerializer<'a> {
    #[deprecated(note = "Renamed to `emit`")]
    /// Emit a value
    pub fn serialize<'b, S: 'b + Value>(self, s: S) -> Result {
        self.emit(s)
    }

    /// Emit a value
    ///
    /// This consumes `self` to prevent serializing one value multiple times
    pub fn emit<'b, S: 'b + Value>(mut self, s: S) -> Result {
        self.done = true;
        s.serialize(self.record, self.key.clone(), self.serializer)
    }
}

impl<'a> Drop for PushFnValueSerializer<'a> {
    fn drop(&mut self) {
        if !self.done {
            // unfortunately this gives no change to return serialization errors
            let _ = self.serializer.emit_unit(self.key.clone());
        }
    }
}

/// Lazy `Value` that writes to Serializer
///
/// It's more ergonomic for closures used as lazy values to return type
/// implementing `Serialize`, but sometimes that forces an allocation (eg.
/// `String`s)
///
/// In some cases it might make sense for another closure form to be used - one
/// taking a serializer as an argument, which avoids lifetimes / allocation
/// issues.
///
/// Generally this method should be used if it avoids a big allocation of
/// `Serialize`-implementing type in performance-critical logging statement.
///
/// ```
/// #[macro_use]
/// extern crate slog;
/// use slog::{PushFnValue, Logger, Discard};
///
/// fn main() {
///     // Create a logger with a key-value printing
///     // `file:line` string value for every logging statement.
///     // `Discard` `Drain` used for brevity.
///     let root = Logger::root(Discard, o!(
///         "source_location" => PushFnValue(|record , s| {
///              s.serialize(
///                   format_args!(
///                        "{}:{}",
///                        record.file(),
///                        record.line(),
///                   )
///              )
///         })
///     ));
/// }
/// ```
pub struct PushFnValue<F>(pub F)
where
    F: 'static + for<'c, 'd> Fn(&'c Record<'d>, PushFnValueSerializer<'c>) -> Result;

impl<F> Value for PushFnValue<F>
where
    F: 'static + for<'c, 'd> Fn(&'c Record<'d>, PushFnValueSerializer<'c>) -> Result,
{
    fn serialize(&self, record: &Record, key: Key, serializer: &mut Serializer) -> Result {
        let ser = PushFnValueSerializer {
            record,
            key,
            serializer,
            done: false,
        };
        (self.0)(record, ser)
    }
}
// }}}

// {{{ KV
/// Key-value pair(s) for log events
///
/// Zero, one or more key value pairs chained together
///
/// Any logging data must implement this trait for slog to be able to use it,
/// although slog comes with default implementations within its macros (the
/// `=>` and `kv!` portions of the log macros).
///
/// If you don't use this trait, you must emit your structured data by
/// specifying both key and value in each log event:
///
/// ```ignore
/// info!(logger, "my event"; "type_key" => %my_val);
/// ```
///
/// If you implement this trait, that can become:
///
/// ```ignore
/// info!(logger, "my event"; my_val);
/// ```
///
/// Types implementing this trait can emit multiple key-value pairs, and can
/// customize their structured representation. The order of emitting them
/// should be consistent with the way key-value pair hierarchy is traversed:
/// from data most specific to the logging context to the most general one. Or
/// in other words: from newest to oldest.
///
/// Implementers are are responsible for calling the `emit_*` methods on the
/// `Serializer` passed in, the `Record` can be used to make display decisions
/// based on context, but for most plain-value structs you will just call
/// `emit_*`.
///
/// # Example
///
/// ```
/// use slog::{KV, Record, Result, Serializer};
///
/// struct MyNewType(i64);
///
/// impl KV for MyNewType {
///    fn serialize(&self, _rec: &Record, serializer: &mut Serializer) -> Result {
///        serializer.emit_i64("my_new_type", self.0)
///    }
/// }
/// ```
///
/// See also [`Value`](trait.Value.html), which allows you to customize just
/// the right hand side of the `=>` structure macro, and (if you have the
/// `nested-values` feature enabled) [`SerdeValue`](trait.SerdeValue.html)
/// which allows emitting anything serde can emit.
pub trait KV {
    /// Serialize self into `Serializer`
    ///
    /// `KV` should call respective `Serializer` methods
    /// for each key-value pair it contains.
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result;
}

impl<'a, T> KV for &'a T
where
    T: KV,
{
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result {
        (**self).serialize(record, serializer)
    }
}

/// Thread-local safety bound for `KV`
///
/// This type is used to enforce `KV`s stored in `Logger`s are thread-safe.
pub trait SendSyncRefUnwindSafeKV: KV + Send + Sync + RefUnwindSafe {}

impl<T> SendSyncRefUnwindSafeKV for T where T: KV + Send + Sync + RefUnwindSafe + ?Sized {}

/// Single pair `Key` and `Value`
pub struct SingleKV<V>(pub Key, pub V)
where
    V: Value;

#[cfg(feature = "dynamic-keys")]
impl<V: Value> From<(String, V)> for SingleKV<V> {
    fn from(x: (String, V)) -> SingleKV<V> {
        SingleKV(Key::from(x.0), x.1)
    }
}
#[cfg(feature = "dynamic-keys")]
impl<V: Value> From<(&'static str, V)> for SingleKV<V> {
    fn from(x: (&'static str, V)) -> SingleKV<V> {
        SingleKV(Key::from(x.0), x.1)
    }
}
#[cfg(not(feature = "dynamic-keys"))]
impl<V: Value> From<(&'static str, V)> for SingleKV<V> {
    fn from(x: (&'static str, V)) -> SingleKV<V> {
        SingleKV(x.0, x.1)
    }
}

impl<V> KV for SingleKV<V>
where
    V: Value,
{
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result {
        self.1.serialize(record, self.0.clone(), serializer)
    }
}

impl KV for () {
    fn serialize(&self, _record: &Record, _serializer: &mut Serializer) -> Result {
        Ok(())
    }
}

impl<T: KV, R: KV> KV for (T, R) {
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result {
        self.0.serialize(record, serializer)?;
        self.1.serialize(record, serializer)
    }
}

impl<T> KV for Box<T>
where
    T: KV + ?Sized,
{
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result {
        (**self).serialize(record, serializer)
    }
}

impl<T> KV for Arc<T>
where
    T: KV + ?Sized,
{
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result {
        (**self).serialize(record, serializer)
    }
}

impl<T> KV for OwnedKV<T>
where
    T: SendSyncRefUnwindSafeKV + ?Sized,
{
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result {
        self.0.serialize(record, serializer)
    }
}

impl<'a> KV for BorrowedKV<'a> {
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result {
        self.0.serialize(record, serializer)
    }
}
// }}}

// {{{ OwnedKV
/// Owned KV
///
/// "Owned" means that the contained data (key-value pairs) can belong
/// to a `Logger` and thus must be thread-safe (`'static`, `Send`, `Sync`)
///
/// Zero, one or more owned key-value pairs.
///
/// Can be constructed with [`o!` macro](macro.o.html).
pub struct OwnedKV<T>(
    #[doc(hidden)]
    /// The exact details of that it are not considered public
    /// and stable API. `slog_o` or `o` macro should be used
    /// instead to create `OwnedKV` instances.
    pub T,
)
where
    T: SendSyncRefUnwindSafeKV + ?Sized;
// }}}

// {{{ BorrowedKV
/// Borrowed `KV`
///
/// "Borrowed" means that the data is only a temporary
/// referenced (`&T`) and can't be stored directly.
///
/// Zero, one or more borrowed key-value pairs.
///
/// Can be constructed with [`b!` macro](macro.b.html).
pub struct BorrowedKV<'a>(
    /// The exact details of it function are not
    /// considered public and stable API. `log` and other
    /// macros should be used instead to create
    /// `BorrowedKV` instances.
    #[doc(hidden)]
    pub &'a KV,
);

// }}}

// {{{ OwnedKVList
struct OwnedKVListNode<T>
where
    T: SendSyncRefUnwindSafeKV + 'static,
{
    next_node: Arc<SendSyncRefUnwindSafeKV + 'static>,
    kv: T,
}

struct MultiListNode {
    next_node: Arc<SendSyncRefUnwindSafeKV + 'static>,
    node: Arc<SendSyncRefUnwindSafeKV + 'static>,
}

/// Chain of `SyncMultiSerialize`-s of a `Logger` and its ancestors
#[derive(Clone)]
pub struct OwnedKVList {
    node: Arc<SendSyncRefUnwindSafeKV + 'static>,
}

impl<T> KV for OwnedKVListNode<T>
where
    T: SendSyncRefUnwindSafeKV + 'static,
{
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result {
        self.kv.serialize(record, serializer)?;
        self.next_node.serialize(record, serializer)?;

        Ok(())
    }
}

impl KV for MultiListNode {
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result {
        self.next_node.serialize(record, serializer)?;
        self.node.serialize(record, serializer)?;

        Ok(())
    }
}

impl KV for OwnedKVList {
    fn serialize(&self, record: &Record, serializer: &mut Serializer) -> Result {
        self.node.serialize(record, serializer)?;

        Ok(())
    }
}

impl fmt::Debug for OwnedKVList {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(")?;
        let mut i = 0;

        {
            let mut as_str_ser = AsFmtSerializer(|key, _val| {
                if i != 0 {
                    write!(f, ", ")?;
                }

                write!(f, "{}", key)?;
                i += 1;
                Ok(())
            });
            let record_static = record_static!(Level::Trace, "");

            self.node
                .serialize(
                    &Record::new(
                        &record_static,
                        &format_args!(""),
                        BorrowedKV(&STATIC_TERMINATOR_UNIT),
                    ),
                    &mut as_str_ser,
                )
                .map_err(|_| fmt::Error)?;
        }

        write!(f, ")")?;
        Ok(())
    }
}

impl OwnedKVList {
    /// New `OwnedKVList` node without a parent (root)
    fn root<T>(values: OwnedKV<T>) -> Self
    where
        T: SendSyncRefUnwindSafeKV + 'static,
    {
        OwnedKVList {
            node: Arc::new(OwnedKVListNode {
                next_node: Arc::new(()),
                kv: values.0,
            }),
        }
    }

    /// New `OwnedKVList` node with an existing parent
    fn new<T>(values: OwnedKV<T>, next_node: Arc<SendSyncRefUnwindSafeKV + 'static>) -> Self
    where
        T: SendSyncRefUnwindSafeKV + 'static,
    {
        OwnedKVList {
            node: Arc::new(OwnedKVListNode {
                next_node,
                kv: values.0,
            }),
        }
    }
}

impl<T> convert::From<OwnedKV<T>> for OwnedKVList
where
    T: SendSyncRefUnwindSafeKV + 'static,
{
    fn from(from: OwnedKV<T>) -> Self {
        OwnedKVList::root(from)
    }
}
// }}}

// {{{ Error
#[derive(Debug)]
/// Serialization Error
pub enum Error {
    /// `io::Error` (not available in ![no_std] mode)
    Io(std::io::Error),
    /// `fmt::Error`
    Fmt(std::fmt::Error),
    /// Other error
    Other,
}

/// Serialization `Result`
pub type Result<T = ()> = result::Result<T, Error>;

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Error {
        Error::Io(err)
    }
}

impl From<core::fmt::Error> for Error {
    fn from(_: core::fmt::Error) -> Error {
        Error::Other
    }
}

impl From<Error> for std::io::Error {
    fn from(e: Error) -> std::io::Error {
        match e {
            Error::Io(e) => e,
            Error::Fmt(_) => std::io::Error::new(std::io::ErrorKind::Other, "formatting error"),
            Error::Other => std::io::Error::new(std::io::ErrorKind::Other, "other error"),
        }
    }
}

impl std::error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::Io(ref e) => e.description(),
            Error::Fmt(_) => "formatting error",
            Error::Other => "serialization error",
        }
    }

    fn cause(&self) -> Option<&std::error::Error> {
        match *self {
            Error::Io(ref e) => Some(e),
            Error::Fmt(ref e) => Some(e),
            Error::Other => None,
        }
    }
}

impl core::fmt::Display for Error {
    fn fmt(&self, fmt: &mut core::fmt::Formatter) -> std::fmt::Result {
        match *self {
            Error::Io(ref e) => e.fmt(fmt),
            Error::Fmt(ref e) => e.fmt(fmt),
            Error::Other => fmt.write_str("Other serialization error"),
        }
    }
}
// }}}

// {{{ Misc
/// This type is here just to abstract away lack of `!` type support in stable
/// rust during time of the release. It will be switched to `!` at some point
/// and `Never` should not be considered "stable" API.
#[doc(hidden)]
pub type Never = private::NeverStruct;

mod private {
    #[doc(hidden)]
    #[derive(Debug)]
    pub struct NeverStruct(());
}

/// This is not part of "stable" API
#[doc(hidden)]
pub static STATIC_TERMINATOR_UNIT: () = ();

#[allow(unknown_lints)]
#[doc(hidden)]
/// Not an API
///
/// Generally it's a bad idea to depend on static logging level
/// in your code. Use closures to perform operations lazily
/// only when logging actually takes place.
pub fn __slog_static_max_level() -> FilterLevel {
    if !cfg!(debug_assertions) {
        if cfg!(feature = "release_max_level_off") {
            return FilterLevel::Off;
        } else if cfg!(feature = "release_max_level_error") {
            return FilterLevel::Error;
        } else if cfg!(feature = "release_max_level_warn") {
            return FilterLevel::Warning;
        } else if cfg!(feature = "release_max_level_info") {
            return FilterLevel::Info;
        } else if cfg!(feature = "release_max_level_debug") {
            return FilterLevel::Debug;
        } else if cfg!(feature = "release_max_level_trace") {
            return FilterLevel::Trace;
        }
    }
    if cfg!(feature = "max_level_off") {
        FilterLevel::Off
    } else if cfg!(feature = "max_level_error") {
        FilterLevel::Error
    } else if cfg!(feature = "max_level_warn") {
        FilterLevel::Warning
    } else if cfg!(feature = "max_level_info") {
        FilterLevel::Info
    } else if cfg!(feature = "max_level_debug") {
        FilterLevel::Debug
    } else if cfg!(feature = "max_level_trace") {
        FilterLevel::Trace
    } else {
        if !cfg!(debug_assertions) {
            FilterLevel::Info
        } else {
            FilterLevel::Debug
        }
    }
}

// }}}

// {{{ Slog v1 Compat
#[deprecated(note = "Renamed to `Value`")]
/// Compatibility name to ease upgrading from `slog v1`
pub type Serialize = Value;

#[deprecated(note = "Renamed to `PushFnValue`")]
/// Compatibility name to ease upgrading from `slog v1`
pub type PushLazy<T> = PushFnValue<T>;

#[deprecated(note = "Renamed to `PushFnValueSerializer`")]
/// Compatibility name to ease upgrading from `slog v1`
pub type ValueSerializer<'a> = PushFnValueSerializer<'a>;

#[deprecated(note = "Renamed to `OwnedKVList`")]
/// Compatibility name to ease upgrading from `slog v1`
pub type OwnedKeyValueList = OwnedKVList;

#[deprecated(note = "Content of ser module moved to main namespace")]
/// Compatibility name to ease upgrading from `slog v1`
pub mod ser {
    #[allow(deprecated)]
    pub use super::{OwnedKeyValueList, PushLazy, Serialize, Serializer, ValueSerializer};
}
// }}}

// {{{ Test
#[cfg(test)]
mod tests {
    use super::{Discard, Logger};

    // Separate module to test lack of imports
    mod no_imports {
        use super::{Discard, Logger};
        /// ensure o! macro expands without error inside a module
        #[test]
        fn test_o_macro_expansion() {
            let _ = Logger::root(Discard, o!("a" => "aa"));
        }
        /// ensure o! macro expands without error inside a module
        #[test]
        fn test_slog_o_macro_expansion() {
            let _ = Logger::root(Discard, slog_o!("a" => "aa"));
        }
    }

    mod std_only {
        use super::super::*;
        use std;

        #[test]
        fn logger_fmt_debug_sanity() {
            let root = Logger::root(Discard, o!("a" => "aa"));
            let log = root.new(o!("b" => "bb", "c" => "cc"));

            assert_eq!(format!("{:?}", log), "Logger(c, b, a)");
        }

        #[test]
        fn multichain() {
            #[derive(Clone)]
            struct CheckOwned;

            impl Drain for CheckOwned {
                type Ok = ();
                type Err = Never;
                fn log(
                    &self,
                    record: &Record,
                    values: &OwnedKVList,
                ) -> std::result::Result<Self::Ok, Self::Err> {
                    assert_eq!(format!("{}", record.msg()), format!("{:?}", values));
                    Ok(())
                }
            }

            let root = Logger::root(CheckOwned, o!("a" => "aa"));
            let log = root.new(o!("b1" => "bb", "b2" => "bb"));

            slog_info!(log, "(b2, b1, a)");

            let log = Logger::root(log, o!("c" => "cc"));
            slog_info!(log, "(c, b2, b1, a)");
            let log = Logger::root(log, o!("d1" => "dd", "d2" => "dd"));
            slog_info!(log, "(d2, d1, c, b2, b1, a)");
        }
    }

    #[allow(clippy::blacklisted_name)]
    #[test]
    fn expressions() {
        use super::{Record, Result, Serializer, KV};

        struct Foo;

        impl Foo {
            fn bar(&self) -> u32 {
                1
            }
        }

        struct X {
            foo: Foo,
        }

        let log = Logger::root(Discard, o!("version" => env!("CARGO_PKG_VERSION")));

        let foo = Foo;
        let r = X { foo };

        slog_warn!(log, "logging message");

        slog_info!(log, #"with tag", "logging message");

        slog_warn!(log, "logging message"; "a" => "b");

        slog_warn!(log, "logging message bar={}", r.foo.bar());

        slog_warn!(
            log,
            "logging message bar={} foo={}",
            r.foo.bar(),
            r.foo.bar()
        );

        // trailing comma check
        slog_warn!(
            log,
            "logging message bar={} foo={}",
            r.foo.bar(),
            r.foo.bar(),
        );

        slog_warn!(log, "logging message bar={}", r.foo.bar(); "x" => 1);

        // trailing comma check
        slog_warn!(log, "logging message bar={}", r.foo.bar(); "x" => 1,);

        slog_warn!(log,
               "logging message bar={}", r.foo.bar();
               "x" => 1, "y" => r.foo.bar());

        slog_warn!(log, "logging message bar={}", r.foo.bar(); "x" => r.foo.bar());

        slog_warn!(log,
               "logging message bar={}", r.foo.bar();
               "x" => r.foo.bar(), "y" => r.foo.bar());

        // trailing comma check
        slog_warn!(log,
               "logging message bar={}", r.foo.bar();
               "x" => r.foo.bar(), "y" => r.foo.bar(),);

        {
            #[derive(Clone)]
            struct K;

            impl KV for K {
                fn serialize(&self, _record: &Record, _serializer: &mut Serializer) -> Result {
                    Ok(())
                }
            }

            let x = K;

            let _log = log.new(o!(x.clone()));
            let _log = log.new(o!("foo" => "bar", x.clone()));
            let _log = log.new(o!("foo" => "bar", x.clone(), x.clone()));
            let _log = log.new(slog_o!("foo" => "bar", x.clone(), x.clone(), "aaa" => "bbb"));

            slog_info!(log, "message"; "foo" => "bar", &x, &x, "aaa" => "bbb");
        }

        slog_info!(
        log,
        "message {}",
          { 3 + 3; 2};
          "foo" => "bar",
          "foo" => { 3 + 3; 2},
          "aaa" => "bbb");
    }

    #[cfg(integer128)]
    #[test]
    fn integer_128_types() {
        let log = Logger::root(Discard, o!("version" => env!("CARGO_PKG_VERSION")));

        slog_info!(log, "i128 = {}", 42i128; "foo" => 7i128);
        slog_info!(log, "u128 = {}", 42u128; "foo" => 7u128);
    }

    #[test]
    fn expressions_fmt() {
        let log = Logger::root(Discard, o!("version" => env!("CARGO_PKG_VERSION")));

        let f = "f";
        let d = (1, 2);

        slog_info!(log, "message"; "f" => %f, "d" => ?d);
    }

    #[test]
    fn makers() {
        use super::*;
        let drain = Duplicate(
            Discard.filter(|r| r.level().is_at_least(Level::Info)),
            Discard.filter_level(Level::Warning),
        )
        .map(Fuse);
        let _log = Logger::root(Arc::new(drain), o!("version" => env!("CARGO_PKG_VERSION")));
    }

    #[test]
    fn simple_logger_erased() {
        use super::*;

        fn takes_arced_drain(_l: Logger) {}

        let drain = Discard.filter_level(Level::Warning).map(Fuse);
        let log = Logger::root_typed(drain, o!("version" => env!("CARGO_PKG_VERSION")));

        takes_arced_drain(log.to_erased());
    }

    #[test]
    fn logger_to_erased() {
        use super::*;

        fn takes_arced_drain(_l: Logger) {}

        let drain = Duplicate(
            Discard.filter(|r| r.level().is_at_least(Level::Info)),
            Discard.filter_level(Level::Warning),
        )
        .map(Fuse);
        let log = Logger::root_typed(drain, o!("version" => env!("CARGO_PKG_VERSION")));

        takes_arced_drain(log.into_erased());
    }

    #[test]
    fn logger_by_ref() {
        use super::*;
        let drain = Discard.filter_level(Level::Warning).map(Fuse);
        let log = Logger::root_typed(drain, o!("version" => env!("CARGO_PKG_VERSION")));
        let f = "f";
        let d = (1, 2);
        slog_info!(&log, "message"; "f" => %f, "d" => ?d);
    }
}

// }}}

// vim: foldmethod=marker foldmarker={{{,}}}

pub mod async_ {
    // {{{ Crate docs
    //! # Async drain for slog-rs
    //!
    //! `slog-rs` is an ecosystem of reusable components for structured, extensible,
    //! composable logging for Rust.
    //!
    //! `slog-async` allows building `Drain`s that offload processing to another
    //! thread.  Typically, serialization and IO operations are slow enough that
    //! they make logging hinder the performance of the main code. Sending log
    //! records to another thread is much faster (ballpark of 100ns).
    //!
    //! Note: Unlike other logging solutions, `slog-rs` does not have a hardcoded
    //! async logging implementation. This crate is just a reasonable reference
    //! implementation. It should have good performance and work well in most use
    //! cases. See the documentation and implementation for more details.
    //!
    //! It's relatively easy to implement your own `slog-rs` async logging. Feel
    //! free to use this one as a starting point.
    //!
    //! ## Beware of `std::process::exit`
    //!
    //! When using `std::process::exit` to terminate a process with an exit code,
    //! it is important to notice that destructors will not be called. This matters
    //! for `slog_async` as it **prevents flushing** of the async drain and
    //! **discards messages** that are not yet written.
    //!
    //! A way around this issue is encapsulate the construction of the logger into
    //! its own function that returns before `std::process::exit` is called.
    //!
    //! ```
    //! // ...
    //! fn main() {
    //!     let _exit_code = run(); // logger gets flushed as `run()` returns.
    //!     // std::process::exit(exit_code) // this needs to be commented or it'll
    //!                                      // end the doctest
    //! }
    //!
    //! fn run() -> i32 {
    //!    // initialize the logger
    //!
    //!    // ... do your thing ...
    //!
    //!    1 // exit code to return
    //! }
    //! ```
    // }}}

    // {{{ Imports & meta
    #![warn(missing_docs)]

    use crossbeam_channel::Sender;

    use super::{BorrowedKV, Level, Record, RecordStatic, SingleKV, KV};
    use super::{Key, OwnedKVList, Serializer};

    use super::Drain;
    use std::error::Error;
    use std::fmt;
    use std::sync;
    use std::{io, thread};

    use std::sync::atomic::AtomicUsize;
    use std::sync::atomic::Ordering;
    use std::sync::Mutex;
    use take_mut::take;
    // }}}

    // {{{ Serializer
    struct ToSendSerializer {
        kv: Box<KV + Send>,
    }

    impl ToSendSerializer {
        fn new() -> Self {
            ToSendSerializer { kv: Box::new(()) }
        }

        fn finish(self) -> Box<KV + Send> {
            self.kv
        }
    }

    impl Serializer for ToSendSerializer {
        fn emit_bool(&mut self, key: Key, val: bool) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_unit(&mut self, key: Key) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, ()))));
            Ok(())
        }
        fn emit_none(&mut self, key: Key) -> super::Result {
            let val: Option<()> = None;
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_char(&mut self, key: Key, val: char) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_u8(&mut self, key: Key, val: u8) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_i8(&mut self, key: Key, val: i8) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_u16(&mut self, key: Key, val: u16) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_i16(&mut self, key: Key, val: i16) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_u32(&mut self, key: Key, val: u32) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_i32(&mut self, key: Key, val: i32) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_f32(&mut self, key: Key, val: f32) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_u64(&mut self, key: Key, val: u64) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_i64(&mut self, key: Key, val: i64) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_f64(&mut self, key: Key, val: f64) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_usize(&mut self, key: Key, val: usize) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_isize(&mut self, key: Key, val: isize) -> super::Result {
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_str(&mut self, key: Key, val: &str) -> super::Result {
            let val = val.to_owned();
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
        fn emit_arguments(&mut self, key: Key, val: &fmt::Arguments) -> super::Result {
            let val = fmt::format(*val);
            take(&mut self.kv, |kv| Box::new((kv, SingleKV(key, val))));
            Ok(())
        }
    }
    // }}}

    // {{{ Async
    // {{{ AsyncError
    /// Errors reported by `Async`
    #[derive(Debug)]
    pub enum AsyncError {
        /// Could not send record to worker thread due to full queue
        Full,
        /// Fatal problem - mutex or channel poisoning issue
        Fatal(Box<std::error::Error>),
    }

    impl<T> From<crossbeam_channel::TrySendError<T>> for AsyncError {
        fn from(_: crossbeam_channel::TrySendError<T>) -> AsyncError {
            AsyncError::Full
        }
    }

    impl<T> From<crossbeam_channel::SendError<T>> for AsyncError {
        fn from(_: crossbeam_channel::SendError<T>) -> AsyncError {
            AsyncError::Fatal(Box::new(io::Error::new(
                io::ErrorKind::BrokenPipe,
                "The logger thread terminated",
            )))
        }
    }

    impl<T> From<std::sync::PoisonError<T>> for AsyncError {
        fn from(err: std::sync::PoisonError<T>) -> AsyncError {
            AsyncError::Fatal(Box::new(io::Error::new(
                io::ErrorKind::BrokenPipe,
                err.description(),
            )))
        }
    }

    /// `AsyncResult` alias
    pub type AsyncResult<T> = std::result::Result<T, AsyncError>;

    // }}}

    // {{{ AsyncCore
    /// `AsyncCore` builder
    pub struct AsyncCoreBuilder<D>
    where
        D: super::Drain<Err = super::Never, Ok = ()> + Send + 'static,
    {
        chan_size: usize,
        blocking: bool,
        drain: D,
        thread_name: Option<String>,
    }

    impl<D> AsyncCoreBuilder<D>
    where
        D: super::Drain<Err = super::Never, Ok = ()> + Send + 'static,
    {
        fn new(drain: D) -> Self {
            AsyncCoreBuilder {
                chan_size: 128,
                blocking: false,
                drain,
                thread_name: None,
            }
        }

        /// Configure a name to be used for the background thread.
        ///
        /// The name must not contain '\0'.
        ///
        /// # Panics
        ///
        /// If a name with '\0' is passed.
        pub fn thread_name(mut self, name: String) -> Self {
            assert!(name.find('\0').is_none(), "Name with \\'0\\' in it passed");
            self.thread_name = Some(name);
            self
        }

        /// Set channel size used to send logging records to worker thread. When
        /// buffer is full `AsyncCore` will start returning `AsyncError::Full` or block, depending on
        /// the `blocking` configuration.
        pub fn chan_size(mut self, s: usize) -> Self {
            self.chan_size = s;
            self
        }

        /// Should the logging call be blocking if the channel is full?
        ///
        /// Default is false, in which case it'll return `AsyncError::Full`.
        pub fn blocking(mut self, blocking: bool) -> Self {
            self.blocking = blocking;
            self
        }

        fn spawn_thread(self) -> (thread::JoinHandle<()>, Sender<AsyncMsg>) {
            let (tx, rx) = crossbeam_channel::bounded(self.chan_size);
            let mut builder = thread::Builder::new();
            if let Some(thread_name) = self.thread_name {
                builder = builder.name(thread_name);
            }
            let drain = self.drain;
            let join = builder
                .spawn(move || loop {
                    match rx.recv().unwrap() {
                        AsyncMsg::Record(r) => {
                            let location = super::RecordLocation {
                                file: &*r.file,
                                line: r.line,
                                column: r.column,
                                function: &*r.function,
                                module: &*r.module,
                            };
                            let rs = RecordStatic {
                                location: &location,
                                level: r.level,
                                tag: &r.tag,
                            };

                            drain
                                .log(
                                    &Record::new(
                                        &rs,
                                        &format_args!("{}", r.msg),
                                        BorrowedKV(&r.kv),
                                    ),
                                    &r.logger_values,
                                )
                                .unwrap();
                        }
                        AsyncMsg::Finish => return,
                    }
                })
                .unwrap();

            (join, tx)
        }

        /// Build `AsyncCore`
        pub fn build(self) -> AsyncCore {
            self.build_no_guard()
        }

        /// Build `AsyncCore`
        pub fn build_no_guard(self) -> AsyncCore {
            let blocking = self.blocking;
            let (join, tx) = self.spawn_thread();

            AsyncCore {
                ref_sender: tx,
                tl_sender: thread_local::ThreadLocal::new(),
                join: Mutex::new(Some(join)),
                blocking,
            }
        }

        /// Build `AsyncCore` with `AsyncGuard`
        ///
        /// See `AsyncGuard` for more information.
        pub fn build_with_guard(self) -> (AsyncCore, AsyncGuard) {
            let blocking = self.blocking;
            let (join, tx) = self.spawn_thread();

            (
                AsyncCore {
                    ref_sender: tx.clone(),
                    tl_sender: thread_local::ThreadLocal::new(),
                    join: Mutex::new(None),
                    blocking,
                },
                AsyncGuard {
                    join: Some(join),
                    tx,
                },
            )
        }
    }

    /// Async guard
    ///
    /// All `Drain`s are reference-counted by every `Logger` that uses them.
    /// `Async` drain runs a worker thread and sends a termination (and flushing)
    /// message only when being `drop`ed. Because of that it's actually
    /// quite easy to have a left-over reference to a `Async` drain, when
    /// terminating: especially on `panic`s or similar unwinding event. Typically
    /// it's caused be a leftover reference like `Logger` in thread-local variable,
    /// global variable, or a thread that is not being joined on. It might be a
    /// program bug, but logging should work reliably especially in case of bugs.
    ///
    /// `AsyncGuard` is a remedy: it will send a flush and termination message to
    /// a `Async` worker thread, and wait for it to finish on it's own `drop`. Using it
    /// is a simplest way to guarantee log flushing when using `slog_async`.
    pub struct AsyncGuard {
        // Should always be `Some`. `None` only
        // after `drop`
        join: Option<thread::JoinHandle<()>>,
        tx: Sender<AsyncMsg>,
    }

    impl Drop for AsyncGuard {
        fn drop(&mut self) {
            let _err: Result<(), Box<std::error::Error>> = {
                || {
                    let _ = self.tx.send(AsyncMsg::Finish);
                    self.join.take().unwrap().join().map_err(|_| {
                        io::Error::new(
                            io::ErrorKind::BrokenPipe,
                            "Logging thread worker join error",
                        )
                    })?;

                    Ok(())
                }
            }();
        }
    }

    /// Core of `Async` drain
    ///
    /// See `Async` for documentation.
    ///
    /// Wrapping `AsyncCore` allows implementing custom overflow (and other errors)
    /// handling strategy.
    ///
    /// Note: On drop `AsyncCore` waits for it's worker-thread to finish (after
    /// handling all previous `Record`s sent to it). If you can't tolerate the
    /// delay, make sure you drop it eg. in another thread.
    pub struct AsyncCore {
        ref_sender: Sender<AsyncMsg>,
        tl_sender: thread_local::ThreadLocal<Sender<AsyncMsg>>,
        join: Mutex<Option<thread::JoinHandle<()>>>,
        blocking: bool,
    }

    impl AsyncCore {
        /// New `AsyncCore` with default parameters
        pub fn new<D>(drain: D) -> Self
        where
            D: super::Drain<Err = super::Never, Ok = ()> + Send + 'static,
            D: std::panic::RefUnwindSafe,
        {
            AsyncCoreBuilder::new(drain).build()
        }

        /// Build `AsyncCore` drain with custom parameters
        pub fn custom<D: super::Drain<Err = super::Never, Ok = ()> + Send + 'static>(
            drain: D,
        ) -> AsyncCoreBuilder<D> {
            AsyncCoreBuilder::new(drain)
        }
        fn get_sender(
            &self,
        ) -> Result<
            &crossbeam_channel::Sender<AsyncMsg>,
            std::sync::PoisonError<sync::MutexGuard<crossbeam_channel::Sender<AsyncMsg>>>,
        > {
            self.tl_sender
                .get_or_try(|| Ok(Box::new(self.ref_sender.clone())))
        }

        /// Send `AsyncRecord` to a worker thread.
        fn send(&self, r: AsyncRecord) -> AsyncResult<()> {
            let sender = self.get_sender()?;

            if self.blocking {
                sender.send(AsyncMsg::Record(r))?;
            } else {
                sender.try_send(AsyncMsg::Record(r))?;
            }

            Ok(())
        }
    }

    impl Drain for AsyncCore {
        type Ok = ();
        type Err = AsyncError;

        fn log(&self, record: &Record, logger_values: &OwnedKVList) -> AsyncResult<()> {
            let mut ser = ToSendSerializer::new();
            record
                .kv()
                .serialize(record, &mut ser)
                .expect("`ToSendSerializer` can't fail");

            self.send(AsyncRecord {
                msg: fmt::format(*record.msg()),
                level: record.level(),
                file: String::from(record.file()),
                line: record.line(),
                column: record.column(),
                function: String::from(record.function()),
                module: String::from(record.module()),
                tag: String::from(record.tag()),
                logger_values: logger_values.clone(),
                kv: ser.finish(),
            })
        }
    }

    struct AsyncRecord {
        msg: String,
        level: Level,
        file: String,
        line: u32,
        column: u32,
        function: String,
        module: String,
        tag: String,
        logger_values: OwnedKVList,
        kv: Box<KV + Send>,
    }

    enum AsyncMsg {
        Record(AsyncRecord),
        Finish,
    }

    impl Drop for AsyncCore {
        fn drop(&mut self) {
            let _err: Result<(), Box<std::error::Error>> = {
                || {
                    if let Some(join) = self.join.lock()?.take() {
                        let _ = self.get_sender()?.send(AsyncMsg::Finish);
                        join.join().map_err(|_| {
                            io::Error::new(
                                io::ErrorKind::BrokenPipe,
                                "Logging thread worker join error",
                            )
                        })?;
                    }
                    Ok(())
                }
            }();
        }
    }
    // }}}

    /// Behavior used when the channel is full.
    ///
    /// # Note
    ///
    /// More variants may be added in the future, without considering it a breaking change.
    #[derive(Copy, Clone, Debug, Eq, PartialEq, Hash, Ord, PartialOrd)]
    pub enum OverflowStrategy {
        /// The message gets dropped and a message with number of dropped is produced once there's
        /// space.
        ///
        /// This is the default.
        ///
        /// Note that the message with number of dropped messages takes one slot in the channel as
        /// well.
        DropAndReport,
        /// The message gets dropped silently.
        Drop,
        /// The caller is blocked until there's enough space.
        Block,
        #[doc(hidden)]
        DoNotMatchAgainstThisAndReadTheDocs,
    }

    /// `Async` builder
    pub struct AsyncBuilder<D>
    where
        D: super::Drain<Err = super::Never, Ok = ()> + Send + 'static,
    {
        core: AsyncCoreBuilder<D>,
        // Increment a counter whenever a message is dropped due to not fitting inside the channel.
        inc_dropped: bool,
    }

    impl<D> AsyncBuilder<D>
    where
        D: super::Drain<Err = super::Never, Ok = ()> + Send + 'static,
    {
        fn new(drain: D) -> AsyncBuilder<D> {
            AsyncBuilder {
                core: AsyncCoreBuilder::new(drain),
                inc_dropped: true,
            }
        }

        /// Set channel size used to send logging records to worker thread. When
        /// buffer is full `AsyncCore` will start returning `AsyncError::Full`.
        pub fn chan_size(self, s: usize) -> Self {
            AsyncBuilder {
                core: self.core.chan_size(s),
                ..self
            }
        }

        /// Sets what will happen if the channel is full.
        pub fn overflow_strategy(self, overflow_strategy: OverflowStrategy) -> Self {
            let (block, inc) = match overflow_strategy {
                OverflowStrategy::Block => (true, false),
                OverflowStrategy::Drop => (false, false),
                OverflowStrategy::DropAndReport => (false, true),
                OverflowStrategy::DoNotMatchAgainstThisAndReadTheDocs => panic!("Invalid variant"),
            };
            AsyncBuilder {
                core: self.core.blocking(block),
                inc_dropped: inc,
            }
        }

        /// Configure a name to be used for the background thread.
        ///
        /// The name must not contain '\0'.
        ///
        /// # Panics
        ///
        /// If a name with '\0' is passed.
        pub fn thread_name(self, name: String) -> Self {
            AsyncBuilder {
                core: self.core.thread_name(name),
                ..self
            }
        }

        /// Complete building `Async`
        pub fn build(self) -> Async {
            Async {
                core: self.core.build_no_guard(),
                dropped: AtomicUsize::new(0),
                inc_dropped: self.inc_dropped,
            }
        }

        /// Complete building `Async`
        pub fn build_no_guard(self) -> Async {
            Async {
                core: self.core.build_no_guard(),
                dropped: AtomicUsize::new(0),
                inc_dropped: self.inc_dropped,
            }
        }

        /// Complete building `Async` with `AsyncGuard`
        ///
        /// See `AsyncGuard` for more information.
        pub fn build_with_guard(self) -> (Async, AsyncGuard) {
            let (core, guard) = self.core.build_with_guard();
            (
                Async {
                    core,
                    dropped: AtomicUsize::new(0),
                    inc_dropped: self.inc_dropped,
                },
                guard,
            )
        }
    }

    /// Async drain
    ///
    /// `Async` will send all the logging records to a wrapped drain running in
    /// another thread.
    ///
    /// `Async` never returns `AsyncError::Full`.
    ///
    /// `Record`s are passed to the worker thread through a channel with a bounded
    /// size (see `AsyncBuilder::chan_size`). On channel overflow `Async` will
    /// start dropping `Record`s and log a message informing about it after
    /// sending more `Record`s is possible again. The exact details of handling
    /// overflow is implementation defined, might change and should not be relied
    /// on, other than message won't be dropped as long as channel does not
    /// overflow.
    ///
    /// Any messages reported by `Async` will contain `slog-async` logging `Record`
    /// tag to allow easy custom handling.
    ///
    /// Note: On drop `Async` waits for it's worker-thread to finish (after handling
    /// all previous `Record`s sent to it). If you can't tolerate the delay, make
    /// sure you drop it eg. in another thread.
    pub struct Async {
        core: AsyncCore,
        dropped: AtomicUsize,
        // Increment the dropped counter if dropped?
        inc_dropped: bool,
    }

    impl Async {
        /// New `AsyncCore` with default parameters
        pub fn default<D: super::Drain<Err = super::Never, Ok = ()> + Send + 'static>(
            drain: D,
        ) -> Self {
            AsyncBuilder::new(drain).build()
        }

        /// Build `Async` drain with custom parameters
        ///
        /// The wrapped drain must handle all results (`Drain<Ok=(),Error=Never>`)
        /// since there's no way to return it back. See `slog::DrainExt::fuse()` and
        /// `slog::DrainExt::ignore_res()` for typical error handling strategies.
        pub fn new<D: super::Drain<Err = super::Never, Ok = ()> + Send + 'static>(
            drain: D,
        ) -> AsyncBuilder<D> {
            AsyncBuilder::new(drain)
        }

        fn push_dropped(&self, logger_values: &OwnedKVList) -> AsyncResult<()> {
            let dropped = self.dropped.swap(0, Ordering::Relaxed);
            if dropped > 0 {
                match self.core.log(
                    &record!(
                        super::Level::Error,
                        "slog-async",
                        &format_args!(
                            "slog-async: logger dropped messages \
                             due to channel \
                             overflow"
                        ),
                        b!("count" => dropped)
                    ),
                    logger_values,
                ) {
                    Ok(()) => {}
                    Err(AsyncError::Full) => {
                        self.dropped.fetch_add(dropped + 1, Ordering::Relaxed);
                        return Ok(());
                    }
                    Err(e) => return Err(e),
                }
            }
            Ok(())
        }
    }

    impl Drain for Async {
        type Ok = ();
        type Err = AsyncError;

        // TODO: Review `Ordering::Relaxed`
        fn log(&self, record: &Record, logger_values: &OwnedKVList) -> AsyncResult<()> {
            self.push_dropped(logger_values)?;

            match self.core.log(record, logger_values) {
                Ok(()) => {}
                Err(AsyncError::Full) if self.inc_dropped => {
                    self.dropped.fetch_add(1, Ordering::Relaxed);
                }
                Err(AsyncError::Full) => {}
                Err(e) => return Err(e),
            }

            Ok(())
        }
    }

    impl Drop for Async {
        fn drop(&mut self) {
            let _ = self.push_dropped(&slog_o!().into());
        }
    }

    // }}}

    // vim: foldmethod=marker foldmarker={{{,}}}
}

pub mod scope_ {
    //! Logging scopes for slog-rs
    //!
    //! Logging scopes are convenience functionality for slog-rs to free user from manually passing
    //! `Logger` objects around.
    //!
    //! Set of macros is also provided as an alternative to original `slog` crate macros, for logging
    //! directly to `Logger` of the current logging scope.
    //!
    //! # Set global logger upfront
    //!
    //! **Warning**: Since `slog-scope` version 4.0.0, `slog-scope` defaults to
    //! panicking on logging if no scope or global logger was set. Because of it, it
    //! is advised to always set a global logger upfront with `set_global_logger`.
    //!
    //! # Using `slog-scope` as a part of API is not advised
    //!
    //! Part of a `slog` logging philosophy is ability to freely express logging contexts
    //! according to logical structure, rather than callstack structure. By using
    //! logging scopes the logging context is tied to code flow again, which is less
    //! expressive.
    //!
    //! It is generally advised **NOT** to use `slog_scope` in libraries. Read more in
    //! [slog-rs FAQ](https://github.com/slog-rs/slog/wiki/FAQ#do-i-have-to-pass-logger-around)
    //!
    //! ```
    //! #[macro_use(slog_o, slog_info, slog_log, slog_record, slog_record_static, slog_b, slog_kv)]
    //! extern crate slog;
    //! #[macro_use]
    //! extern crate slog_scope;
    //! extern crate slog_term;
    //!
    //! use slog::Drain;
    //!
    //! fn foo() {
    //!     slog_info!(slog_scope::logger(), "foo");
    //!     info!("foo"); // Same as above, but more ergonomic and a bit faster
    //!                   // since it uses `with_logger`
    //! }
    //!
    //! fn main() {
    //!     let plain = slog_term::PlainSyncDecorator::new(std::io::stdout());
    //!     let log = slog::Logger::root(
    //!         slog_term::FullFormat::new(plain)
    //!         .build().fuse(), slog_o!()
    //!     );
    //!
    //!     // Make sure to save the guard, see documentation for more information
    //!     let _guard = slog_scope::set_global_logger(log);
    //!     slog_scope::scope(&slog_scope::logger().new(slog_o!("scope" => "1")),
    //!         || foo()
    //!     );
    //! }

    #![warn(missing_docs)]

    use super::{Logger, OwnedKVList, Record};

    use crossbeam::atomic::ArcCell;
    use std::cell::RefCell;
    use std::sync::Arc;

    use std::result;

    /// Log a critical level message using current scope logger
    #[macro_export]
    macro_rules! crit( ($($args:tt)+) => {
    $crate::slog::scope_::with_logger(|logger| slog_crit![logger, $($args)+])
    };);
    /// Log a error level message using current scope logger
    #[macro_export]
    macro_rules! error( ($($args:tt)+) => {
    $crate::slog::scope_::with_logger(|logger| slog_error![logger, $($args)+])
    };);
    /// Log a warning level message using current scope logger
    #[macro_export]
    macro_rules! warn( ($($args:tt)+) => {
    $crate::slog::scope_::with_logger(|logger| slog_warn![logger, $($args)+])
    };);
    /// Log a info level message using current scope logger
    #[macro_export]
    macro_rules! info( ($($args:tt)+) => {
    $crate::slog::scope_::with_logger(|logger| slog_info![logger, $($args)+])
    };);
    /// Log a debug level message using current scope logger
    #[macro_export]
    macro_rules! debug( ($($args:tt)+) => {
    $crate::slog::scope_::with_logger(|logger| slog_debug![logger, $($args)+])
    };);
    /// Log a trace level message using current scope logger
    #[macro_export]
    macro_rules! trace( ($($args:tt)+) => {
    $crate::slog::scope_::with_logger(|logger| slog_trace![logger, $($args)+])
    };);

    thread_local! {
        static TL_SCOPES: RefCell<Vec<*const super::Logger>> = RefCell::new(Vec::with_capacity(8))
    }

    lazy_static! {
        static ref GLOBAL_LOGGER: ArcCell<super::Logger> =
            ArcCell::new(Arc::new(super::Logger::root(super::Discard, o!())));
    }

    struct NoGlobalLoggerSet;

    impl super::Drain for NoGlobalLoggerSet {
        type Ok = ();
        type Err = super::Never;

        fn log(
            &self,
            _record: &Record,
            _values: &OwnedKVList,
        ) -> result::Result<Self::Ok, Self::Err> {
            panic!(
                "slog-scope: No logger set. Use `slog_scope::set_global_logger` or `slog_scope::scope`."
            )
        }
    }

    /// Guard resetting global logger
    ///
    /// On drop it will reset global logger to `slog::Discard`.
    /// This will `drop` any existing global logger.
    #[must_use]
    pub struct GlobalLoggerGuard {
        canceled: bool,
    }

    impl GlobalLoggerGuard {
        fn new() -> Self {
            GlobalLoggerGuard { canceled: false }
        }

        /// Cancel resetting global logger
        pub fn cancel_reset(mut self) {
            self.canceled = true;
        }
    }

    impl Drop for GlobalLoggerGuard {
        fn drop(&mut self) {
            if !self.canceled {
                let _ = GLOBAL_LOGGER.set(Arc::new(super::Logger::root(NoGlobalLoggerSet, o!())));
            }
        }
    }

    /// Set global `Logger` that is returned by calls like `logger()` outside of any logging scope.
    pub fn set_global_logger(l: super::Logger) -> GlobalLoggerGuard {
        let _ = GLOBAL_LOGGER.set(Arc::new(l));

        GlobalLoggerGuard::new()
    }

    struct ScopeGuard;

    impl ScopeGuard {
        fn new(logger: &super::Logger) -> Self {
            TL_SCOPES.with(|s| {
                s.borrow_mut().push(logger as *const Logger);
            });

            ScopeGuard
        }
    }

    impl Drop for ScopeGuard {
        fn drop(&mut self) {
            TL_SCOPES.with(|s| {
                s.borrow_mut()
                    .pop()
                    .expect("TL_SCOPES should contain a logger");
            })
        }
    }

    /// Access the `Logger` for the current logging scope
    ///
    /// This function needs to clone an underlying scoped
    /// `Logger`. If performance is of vital importance,
    /// use `with_logger`.
    pub fn logger() -> Logger {
        TL_SCOPES.with(|s| {
            let s = s.borrow();
            match s.last() {
                Some(logger) => (unsafe { &**logger }).clone(),
                None => (*GLOBAL_LOGGER.get()).clone(),
            }
        })
    }

    /// Access the `Logger` for the current logging scope
    ///
    /// This function doesn't have to clone the Logger
    /// so it might be a bit faster.
    pub fn with_logger<F, R>(f: F) -> R
    where
        F: FnOnce(&Logger) -> R,
    {
        TL_SCOPES.with(|s| {
            let s = s.borrow();
            match s.last() {
                Some(logger) => f(unsafe { &**logger }),
                None => f(&(*GLOBAL_LOGGER.get())),
            }
        })
    }

    /// Execute code in a logging scope
    ///
    /// Logging scopes allow using a `slog::Logger` without explicitly
    /// passing it in the code.
    ///
    /// At any time current active `Logger` for a given thread can be retrived
    /// with `logger()` call.
    ///
    /// Logging scopes can be nested and are panic safe.
    ///
    /// `logger` is the `Logger` to use during the duration of `f`.
    /// `with_current_logger` can be used to build it as a child of currently active
    /// logger.
    ///
    /// `f` is a code to be executed in the logging scope.
    ///
    /// Note: Thread scopes are thread-local. Each newly spawned thread starts
    /// with a global logger, as a current logger.
    pub fn scope<SF, R>(logger: &super::Logger, f: SF) -> R
    where
        SF: FnOnce() -> R,
    {
        let _guard = ScopeGuard::new(&logger);
        f()
    }
}

pub mod stdlog_ {
    //! `log` crate adapter for slog-rs
    //!
    //! This crate provides two way compatibility with Rust standard `log` crate.
    //!
    //! ### `log` -> `slog`
    //!
    //! After calling `init()` `slog-stdlog` will take a role of `log` crate
    //! back-end, forwarding all the `log` logging to `slog_scope::logger()`.
    //! In other words, any `log` crate logging statement will behave like it was `slog`
    //! logging statement executed with logger returned by `slog_scope::logger()`.
    //!
    //! See documentation of `slog-scope` for more information about logging scopes.
    //!
    //! See [`init` documentation](fn.init.html) for an example.
    //!
    //! ### `slog` -> `log`
    //!
    //! `StdLog` is `slog::Drain` that will pass all `Record`s passing through it to
    //! `log` crate just like they were crated with `log` crate logging macros in
    //! the first place.
    //!
    //! ## `slog-scope`
    //!
    //! Since `log` does not have any form of context, and does not support `Logger`
    //! `slog-stdlog` relies on "logging scopes" to establish it.
    //!
    //! You must set up logging context for `log` -> `slog` via `slog_scope::scope`
    //! or `slog_scope::set_global_logger`. Setting a global logger upfront via
    //! `slog_scope::set_global_logger` is highly recommended.
    //!
    //! Note: Since `slog-stdlog` v2, unlike previous releases, `slog-stdlog` uses
    //! logging scopes provided by `slog-scope` crate instead of it's own.
    //!
    //! Refer to `slog-scope` crate documentation for more information.
    //!
    //! ### Warning
    //!
    //! Be careful when using both methods at the same time, as a loop can be easily
    //! created: `log` -> `slog` -> `log` -> ...
    //!
    //! ## Compile-time log level filtering
    //!
    //! For filtering `debug!` and other `log` statements at compile-time, configure
    //! the features on the `log` crate in your `Cargo.toml`:
    //!
    //! ```norust
    //! log = { version = "*", features = ["max_level_trace", "release_max_level_warn"] }
    //! ```
    #![warn(missing_docs)]

    use log::Metadata;
    use std::{fmt, io};

    use super::Level;
    use super::KV;

    struct Logger;

    fn log_to_slog_level(level: log::Level) -> Level {
        match level {
            log::Level::Trace => Level::Trace,
            log::Level::Debug => Level::Debug,
            log::Level::Info => Level::Info,
            log::Level::Warn => Level::Warning,
            log::Level::Error => Level::Error,
        }
    }

    impl log::Log for Logger {
        fn enabled(&self, _: &Metadata) -> bool {
            true
        }

        fn log(&self, r: &log::Record) {
            let level = log_to_slog_level(r.metadata().level());

            let args = r.args();
            let target = r.target();

            let s = super::RecordStatic {
                location: &super::RecordLocation {
                    file: r.file().unwrap_or("<unknown>"),
                    line: 0,
                    column: 0,
                    function: "<unknown>",
                    module: r.module_path().unwrap_or("<unknown>"),
                },
                level,
                tag: target,
            };
            super::scope_::with_logger(|logger| logger.log(&super::Record::new(&s, args, b!())))
        }

        fn flush(&self) {
            //nothing buffered
        }
    }

    /// Register `slog-stdlog` as `log` backend.
    ///
    /// This will pass all logging statements crated with `log`
    /// crate to current `slog-scope::logger()`.
    ///
    /// ```
    /// #[macro_use]
    /// extern crate log;
    /// #[macro_use(slog_o, slog_kv)]
    /// extern crate slog;
    /// extern crate slog_stdlog;
    /// extern crate slog_scope;
    /// extern crate slog_term;
    /// extern crate slog_async;
    ///
    /// use slog::Drain;
    ///
    /// fn main() {
    ///     let decorator = slog_term::TermDecorator::new().build();
    ///     let drain = slog_term::FullFormat::new(decorator).build().fuse();
    ///     let drain = slog_async::Async::new(drain).build().fuse();
    ///     let logger = slog::Logger::root(drain, slog_o!("version" => env!("CARGO_PKG_VERSION")));
    ///
    ///     let _scope_guard = slog_scope::set_global_logger(logger);
    ///     let _log_guard = slog_stdlog::init().unwrap();
    ///     // Note: this `info!(...)` macro comes from `log` crate
    ///     info!("standard logging redirected to slog");
    /// }
    /// ```
    pub fn init() -> Result<(), log::SetLoggerError> {
        log::set_max_level(log::LevelFilter::max());
        log::set_boxed_logger(Box::new(Logger))
    }

    /// Drain logging `Record`s into `log` crate
    ///
    /// Any `Record` passing through this `Drain` will be forwarded
    /// to `log` crate, just like it was created with `log` crate macros
    /// in the first place. The message and key-value pairs will be formated
    /// to be one string.
    ///
    /// Caution needs to be taken to prevent circular loop where `Logger`
    /// installed via `slog-stdlog::set_logger` would log things to a `StdLog`
    /// drain, which would again log things to the global `Logger` and so on
    /// leading to an infinite recursion.
    pub struct StdLog;

    struct LazyLogString<'a> {
        info: &'a super::Record<'a>,
        logger_values: &'a super::OwnedKVList,
    }

    impl<'a> LazyLogString<'a> {
        fn new(info: &'a super::Record, logger_values: &'a super::OwnedKVList) -> Self {
            LazyLogString {
                info,
                logger_values,
            }
        }
    }

    impl<'a> fmt::Display for LazyLogString<'a> {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            write!(f, "{}", self.info.msg())?;

            let io = io::Cursor::new(Vec::new());
            let mut ser = KSV::new(io);

            let res = {
                || -> io::Result<()> {
                    self.logger_values.serialize(self.info, &mut ser)?;
                    self.info.kv().serialize(self.info, &mut ser)?;
                    Ok(())
                }
            }()
            .map_err(|_| fmt::Error);

            res?;

            let values = ser.into_inner().into_inner();

            write!(f, "{}", String::from_utf8_lossy(&values))
        }
    }

    impl super::Drain for StdLog {
        type Err = io::Error;
        type Ok = ();
        fn log(&self, info: &super::Record, logger_values: &super::OwnedKVList) -> io::Result<()> {
            let level = match info.level() {
                super::Level::Critical | super::Level::Error => log::Level::Error,
                super::Level::Warning => log::Level::Warn,
                super::Level::Info => log::Level::Info,
                super::Level::Debug => log::Level::Debug,
                super::Level::Trace => log::Level::Trace,
            };

            let mut target = info.tag();
            if target.is_empty() {
                target = info.module();
            }

            let r = log::RecordBuilder::new()
                .level(level)
                .target(target)
                .module_path(Some(info.module()))
                .file(Some(info.file()))
                .line(Some(info.line()))
                .build();
            log::logger().log(&r);

            Ok(())
        }
    }

    /// Key-Separator-Value serializer
    struct KSV<W: io::Write> {
        io: W,
    }

    impl<W: io::Write> KSV<W> {
        fn new(io: W) -> Self {
            KSV { io }
        }

        fn into_inner(self) -> W {
            self.io
        }
    }

    impl<W: io::Write> super::Serializer for KSV<W> {
        fn emit_arguments(&mut self, key: super::Key, val: &fmt::Arguments) -> super::Result {
            write!(self.io, ", {}: {}", key, val)?;
            Ok(())
        }
    }
}

pub mod term_ {
    // {{{ Module docs
    //! `slog-rs`'s `Drain` for terminal output
    //!
    //! This crate implements output formatting targeting logging to
    //! terminal/console/shell or similar text-based IO.
    //!
    //! **Warning**: `slog-term` (like `slog-rs` itself) is fast, modular and
    //! extensible.  It comes with a price: a lot of details (*that you don't care
    //! about
    //! right now and think they are stupid, until you actually do and then you are
    //! happy that someone thought of them for you*) are being taken into
    //! consideration. Anyway, **if you just want to get a logging to terminal
    //! working with `slog`**, consider using a wrapper crate like
    //! [sloggers](https://docs.rs/sloggers/) instead.
    //!
    //! **Note**: A lot of users gets bitten by the fact that
    //! `slog::Logger::root(...)` requires a drain that is
    //! safe to send and share across threads (`Send+Sync`). With shared resource
    //! like terminal or a file to which you log, a synchronization needs to be
    //! taken care of. If you get compilation errors around `Sync` or `Send` you
    //! are doing something wrong around it.
    //!
    //! Using `Decorator` open trait, user can implement outputting
    //! using different colors, terminal types and so on.
    //!
    //! # Synchronization via `PlainSyncDecorator`
    //!
    //! This logger works by synchronizing on the IO directly in
    //! `PlainSyncDecorator`.  The formatting itself is thread-safe.
    //!
    //! ```
    //! #[macro_use]
    //! extern crate slog;
    //! extern crate slog_term;
    //!
    //! use slog::*;
    //!
    //! fn main() {
    //!     let plain = slog_term::PlainSyncDecorator::new(std::io::stdout());
    //!     let logger = Logger::root(
    //!         slog_term::FullFormat::new(plain)
    //!         .build().fuse(), o!()
    //!     );
    //!
    //!     info!(logger, "Logging ready!");
    //! }
    //! ```
    //!
    //! # Synchronization via `slog_async`
    //!
    //! This drain puts logging into a separate thread via `slog_async::Async`:
    //! formatting and writing to terminal is happening in a one dedicated thread,
    //! so no further synchronization is required.
    //!
    //! ```
    //! #[macro_use]
    //! extern crate slog;
    //! extern crate slog_term;
    //! extern crate slog_async;
    //!
    //! use slog::Drain;
    //!
    //! fn main() {
    //!     let decorator = slog_term::TermDecorator::new().build();
    //!     let drain = slog_term::CompactFormat::new(decorator).build().fuse();
    //!     let drain = slog_async::Async::new(drain).build().fuse();
    //!
    //!     let log = slog::Logger::root(drain, o!());
    //!
    //!     info!(log, "Logging ready!");
    //! }
    //! ```
    //!
    //! # Synchronization via `Mutex`
    //!
    //! This drain synchronizes by wrapping everything in a big mutex (yes,
    //! `Mutex<Drain>` implements a `Drain` trait). This is kind of slow, but in
    //! scripting languages like Ruby or Python pretty much the whole code is
    //! running in a one
    //! huge mutex and noone seems to mind, so I'm sure you're going to get away
    //! with this. Personally, I am a bit sad, that I've spent so much effort to
    //! give you tools to make your code as efficient as possible, and you choose
    //! this. ಠ_ಠ . But I'm here to serve, not to tell you what to do.
    //!
    //! ```
    //! #[macro_use]
    //! extern crate slog;
    //! extern crate slog_term;
    //!
    //! use slog::Drain;
    //!
    //! fn main() {
    //!     let decorator = slog_term::TermDecorator::new().build();
    //!     let drain = slog_term::CompactFormat::new(decorator).build();
    //!     let drain = std::sync::Mutex::new(drain).fuse();
    //!
    //!     let log = slog::Logger::root(drain, o!());
    //!
    //!     info!(log, "Logging ready!");
    //! }
    //! ```
    // }}}

    // {{{ Imports & meta
    #![warn(missing_docs)]

    use super::Drain;
    use super::Key;
    use super::*;
    use std::cell::RefCell;
    use std::io::Write as IoWrite;
    use std::panic::{RefUnwindSafe, UnwindSafe};
    use std::result;
    use std::{fmt, io, mem, sync};
    // }}}

    // {{{ Decorator
    /// Output decorator
    ///
    /// Trait implementing strategy of output formating in terms of IO,
    /// colors, etc.
    pub trait Decorator {
        /// Get a `RecordDecorator` for a given `record`
        ///
        /// This allows `Decorator` to have on-stack data per processed `Record`s
        ///
        fn with_record<F>(
            &self,
            _record: &Record,
            _logger_values: &OwnedKVList,
            f: F,
        ) -> io::Result<()>
        where
            F: FnOnce(&mut RecordDecorator) -> io::Result<()>;
    }

    impl<T: ?Sized> Decorator for Box<T>
    where
        T: Decorator,
    {
        fn with_record<F>(&self, record: &Record, logger_kv: &OwnedKVList, f: F) -> io::Result<()>
        where
            F: FnOnce(&mut RecordDecorator) -> io::Result<()>,
        {
            (**self).with_record(record, logger_kv, f)
        }
    }

    /// Per-record decorator
    pub trait RecordDecorator: io::Write {
        /// Reset formatting to defaults
        fn reset(&mut self) -> io::Result<()>;

        /// Format normal text
        fn start_whitespace(&mut self) -> io::Result<()> {
            self.reset()
        }

        /// Format `Record` message
        fn start_msg(&mut self) -> io::Result<()> {
            self.reset()
        }

        /// Format timestamp
        fn start_timestamp(&mut self) -> io::Result<()> {
            self.reset()
        }

        /// Format `Record` level
        fn start_level(&mut self) -> io::Result<()> {
            self.reset()
        }

        /// Format a comma between key-value pairs
        fn start_comma(&mut self) -> io::Result<()> {
            self.reset()
        }

        /// Format key
        fn start_key(&mut self) -> io::Result<()> {
            self.reset()
        }

        /// Format a value
        fn start_value(&mut self) -> io::Result<()> {
            self.reset()
        }

        /// Format value
        fn start_separator(&mut self) -> io::Result<()> {
            self.reset()
        }
    }

    impl RecordDecorator for Box<RecordDecorator> {
        fn reset(&mut self) -> io::Result<()> {
            (**self).reset()
        }
        fn start_whitespace(&mut self) -> io::Result<()> {
            (**self).start_whitespace()
        }

        /// Format `Record` message
        fn start_msg(&mut self) -> io::Result<()> {
            (**self).start_msg()
        }

        /// Format timestamp
        fn start_timestamp(&mut self) -> io::Result<()> {
            (**self).start_timestamp()
        }

        /// Format `Record` level
        fn start_level(&mut self) -> io::Result<()> {
            (**self).start_level()
        }

        /// Format `Record` message
        fn start_comma(&mut self) -> io::Result<()> {
            (**self).start_comma()
        }

        /// Format key
        fn start_key(&mut self) -> io::Result<()> {
            (**self).start_key()
        }

        /// Format value
        fn start_value(&mut self) -> io::Result<()> {
            (**self).start_value()
        }

        /// Format value
        fn start_separator(&mut self) -> io::Result<()> {
            (**self).start_separator()
        }
    }
    // }}}

    // {{{ Misc
    /// Returns `true` if message was not empty
    fn print_msg_header(
        fn_timestamp: &ThreadSafeTimestampFn<Output = io::Result<()>>,
        mut rd: &mut RecordDecorator,
        record: &Record,
    ) -> io::Result<bool> {
        rd.start_timestamp()?;
        fn_timestamp(&mut rd)?;

        rd.start_whitespace()?;
        write!(rd, " ")?;

        rd.start_level()?;
        write!(rd, "{}", record.level().as_short_str())?;

        rd.start_whitespace()?;
        write!(rd, " ")?;

        rd.start_msg()?;
        let mut count_rd = CountingWriter::new(&mut rd);
        write!(count_rd, "{}", record.msg())?;
        Ok(count_rd.count() != 0)
    }

    // }}}

    // {{{ Term
    /// Terminal-output formatting `Drain`
    ///
    /// **Note**: logging to `FullFormat` drain is thread-safe, since every
    /// line of output is formatted indecently. However, the underlying
    /// IO, needs to be synchronized.
    pub struct FullFormat<D>
    where
        D: Decorator,
    {
        decorator: D,
        fn_timestamp: Box<ThreadSafeTimestampFn<Output = io::Result<()>>>,
        use_original_order: bool,
    }

    /// Streamer builder
    pub struct FullFormatBuilder<D>
    where
        D: Decorator,
    {
        decorator: D,
        fn_timestamp: Box<ThreadSafeTimestampFn<Output = io::Result<()>>>,
        original_order: bool,
    }

    impl<D> FullFormatBuilder<D>
    where
        D: Decorator,
    {
        /// Use the UTC time zone for the timestamp
        pub fn use_utc_timestamp(mut self) -> Self {
            self.fn_timestamp = Box::new(timestamp_utc);
            self
        }

        /// Use the local time zone for the timestamp (default)
        pub fn use_local_timestamp(mut self) -> Self {
            self.fn_timestamp = Box::new(timestamp_local);
            self
        }

        /// Provide a custom function to generate the timestamp
        pub fn use_custom_timestamp<F>(mut self, f: F) -> Self
        where
            F: ThreadSafeTimestampFn,
        {
            self.fn_timestamp = Box::new(f);
            self
        }

        /// Use the original ordering of key-value pairs
        ///
        /// By default, key-values are printed in a reversed order. This option will
        /// change it to the order in which key-values were added.
        pub fn use_original_order(mut self) -> Self {
            self.original_order = true;
            self
        }

        /// Build `FullFormat`
        pub fn build(self) -> FullFormat<D> {
            FullFormat {
                decorator: self.decorator,
                fn_timestamp: self.fn_timestamp,
                use_original_order: self.original_order,
            }
        }
    }

    impl<D> Drain for FullFormat<D>
    where
        D: Decorator,
    {
        type Ok = ();
        type Err = io::Error;

        fn log(
            &self,
            record: &Record,
            values: &OwnedKVList,
        ) -> result::Result<Self::Ok, Self::Err> {
            self.format_full(record, values)
        }
    }

    impl<D> FullFormat<D>
    where
        D: Decorator,
    {
        /// New `TermBuilder`
        pub fn new(d: D) -> FullFormatBuilder<D> {
            FullFormatBuilder {
                fn_timestamp: Box::new(timestamp_local),
                decorator: d,
                original_order: false,
            }
        }

        fn format_full(&self, record: &Record, values: &OwnedKVList) -> io::Result<()> {
            self.decorator.with_record(record, values, |decorator| {
                let comma_needed = print_msg_header(&*self.fn_timestamp, decorator, record)?;
                {
                    let mut serializer =
                        Serializer::new(decorator, comma_needed, self.use_original_order);

                    record.kv().serialize(record, &mut serializer)?;

                    values.serialize(record, &mut serializer)?;

                    serializer.finish()?;
                }

                decorator.start_whitespace()?;
                writeln!(decorator)?;

                decorator.flush()?;

                Ok(())
            })
        }
    }
    // }}}

    // {{{ CompactFormat
    /// Compact terminal-output formatting `Drain`
    ///
    /// **Note**: Compact logging format is not `Sync` (thread-safe) and needs to be
    /// synchronized externally, as current output depends on the previous one.
    ///
    /// Put it into a `std::sync::Mutex` or `slog_async::Async` worker-thread to
    /// serialize accesses to it.
    pub struct CompactFormat<D>
    where
        D: Decorator,
    {
        decorator: D,
        history: RefCell<Vec<(Vec<u8>, Vec<u8>)>>,
        fn_timestamp: Box<ThreadSafeTimestampFn<Output = io::Result<()>>>,
    }

    /// Streamer builder
    pub struct CompactFormatBuilder<D>
    where
        D: Decorator,
    {
        decorator: D,
        fn_timestamp: Box<ThreadSafeTimestampFn<Output = io::Result<()>>>,
    }

    impl<D> CompactFormatBuilder<D>
    where
        D: Decorator,
    {
        /// Use the UTC time zone for the timestamp
        pub fn use_utc_timestamp(mut self) -> Self {
            self.fn_timestamp = Box::new(timestamp_utc);
            self
        }

        /// Use the local time zone for the timestamp (default)
        pub fn use_local_timestamp(mut self) -> Self {
            self.fn_timestamp = Box::new(timestamp_local);
            self
        }

        /// Provide a custom function to generate the timestamp
        pub fn use_custom_timestamp<F>(mut self, f: F) -> Self
        where
            F: ThreadSafeTimestampFn,
        {
            self.fn_timestamp = Box::new(f);
            self
        }

        /// Build the streamer
        pub fn build(self) -> CompactFormat<D> {
            CompactFormat {
                decorator: self.decorator,
                fn_timestamp: self.fn_timestamp,
                history: RefCell::new(vec![]),
            }
        }
    }

    impl<D> Drain for CompactFormat<D>
    where
        D: Decorator,
    {
        type Ok = ();
        type Err = io::Error;

        fn log(
            &self,
            record: &Record,
            values: &OwnedKVList,
        ) -> result::Result<Self::Ok, Self::Err> {
            self.format_compact(record, values)
        }
    }

    impl<D> CompactFormat<D>
    where
        D: Decorator,
    {
        /// New `CompactFormatBuilder`
        pub fn new(d: D) -> CompactFormatBuilder<D> {
            CompactFormatBuilder {
                fn_timestamp: Box::new(timestamp_local),
                decorator: d,
            }
        }

        fn format_compact(&self, record: &Record, values: &OwnedKVList) -> io::Result<()> {
            self.decorator.with_record(record, values, |decorator| {
                let indent = {
                    let mut history_ref = self.history.borrow_mut();
                    let mut serializer = CompactFormatSerializer::new(decorator, &mut *history_ref);

                    values.serialize(record, &mut serializer)?;

                    serializer.finish()?
                };

                decorator.start_whitespace()?;

                for _ in 0..indent {
                    write!(decorator, " ")?;
                }

                let comma_needed = print_msg_header(&*self.fn_timestamp, decorator, record)?;
                {
                    let mut serializer = Serializer::new(decorator, comma_needed, false);

                    record.kv().serialize(record, &mut serializer)?;

                    serializer.finish()?;
                }

                decorator.start_whitespace()?;
                writeln!(decorator)?;

                decorator.flush()?;

                Ok(())
            })
        }
    }
    // }}}

    // {{{ Serializer
    struct Serializer<'a> {
        comma_needed: bool,
        decorator: &'a mut RecordDecorator,
        reverse: bool,
        stack: Vec<(String, String)>,
    }

    impl<'a> Serializer<'a> {
        fn new(d: &'a mut RecordDecorator, comma_needed: bool, reverse: bool) -> Self {
            Serializer {
                comma_needed,
                decorator: d,
                reverse,
                stack: vec![],
            }
        }

        fn maybe_print_comma(&mut self) -> io::Result<()> {
            if self.comma_needed {
                self.decorator.start_comma()?;
                write!(self.decorator, ", ")?;
            }
            self.comma_needed |= true;
            Ok(())
        }

        fn finish(mut self) -> io::Result<()> {
            loop {
                if let Some((k, v)) = self.stack.pop() {
                    self.maybe_print_comma()?;
                    self.decorator.start_key()?;
                    write!(self.decorator, "{}", k)?;
                    write!(self.decorator, ":")?;
                    self.decorator.start_whitespace()?;
                    write!(self.decorator, " ")?;
                    self.decorator.start_value()?;
                    write!(self.decorator, "{}", v)?;
                } else {
                    return Ok(());
                }
            }
        }
    }

    impl<'a> Drop for Serializer<'a> {
        fn drop(&mut self) {
            if !self.stack.is_empty() {
                panic!("stack not empty");
            }
        }
    }

    macro_rules! s(
    ($s:expr, $k:expr, $v:expr) => {

        if $s.reverse {
            $s.stack.push(($k.into(), format!("{}", $v)));
        } else {
        $s.maybe_print_comma()?;
        $s.decorator.start_key()?;
        write!($s.decorator, "{}", $k)?;
        $s.decorator.start_separator()?;
        write!($s.decorator, ":")?;
        $s.decorator.start_whitespace()?;
        write!($s.decorator, " ")?;
        $s.decorator.start_value()?;
        write!($s.decorator, "{}", $v)?;
        }
    };
);

    impl<'a> super::ser::Serializer for Serializer<'a> {
        fn emit_none(&mut self, key: Key) -> super::Result {
            s!(self, key, "None");
            Ok(())
        }
        fn emit_unit(&mut self, key: Key) -> super::Result {
            s!(self, key, "()");
            Ok(())
        }

        fn emit_bool(&mut self, key: Key, val: bool) -> super::Result {
            s!(self, key, val);
            Ok(())
        }

        fn emit_char(&mut self, key: Key, val: char) -> super::Result {
            s!(self, key, val);
            Ok(())
        }

        fn emit_usize(&mut self, key: Key, val: usize) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_isize(&mut self, key: Key, val: isize) -> super::Result {
            s!(self, key, val);
            Ok(())
        }

        fn emit_u8(&mut self, key: Key, val: u8) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_i8(&mut self, key: Key, val: i8) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_u16(&mut self, key: Key, val: u16) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_i16(&mut self, key: Key, val: i16) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_u32(&mut self, key: Key, val: u32) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_i32(&mut self, key: Key, val: i32) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_f32(&mut self, key: Key, val: f32) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_u64(&mut self, key: Key, val: u64) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_i64(&mut self, key: Key, val: i64) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_f64(&mut self, key: Key, val: f64) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_str(&mut self, key: Key, val: &str) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
        fn emit_arguments(&mut self, key: Key, val: &fmt::Arguments) -> super::Result {
            s!(self, key, val);
            Ok(())
        }
    }
    // }}}

    // {{{ CompactFormatSerializer

    struct CompactFormatSerializer<'a> {
        decorator: &'a mut RecordDecorator,
        history: &'a mut Vec<(Vec<u8>, Vec<u8>)>,
        buf: Vec<(Vec<u8>, Vec<u8>)>,
    }

    impl<'a> CompactFormatSerializer<'a> {
        fn new(d: &'a mut RecordDecorator, history: &'a mut Vec<(Vec<u8>, Vec<u8>)>) -> Self {
            CompactFormatSerializer {
                decorator: d,
                history,
                buf: vec![],
            }
        }

        fn finish(&mut self) -> io::Result<usize> {
            let mut indent = 0;

            for mut buf in self.buf.drain(..).rev() {
                let (print, trunc, push) = if let Some(prev) = self.history.get_mut(indent) {
                    if *prev != buf {
                        *prev = mem::replace(&mut buf, (vec![], vec![]));
                        (true, true, false)
                    } else {
                        (false, false, false)
                    }
                } else {
                    (true, false, true)
                };

                if push {
                    self.history.push(mem::replace(&mut buf, (vec![], vec![])));
                }

                if trunc {
                    self.history.truncate(indent + 1);
                }

                if print {
                    let &(ref k, ref v) = self.history.get(indent).expect("assertion failed");
                    self.decorator.start_whitespace()?;
                    for _ in 0..indent {
                        write!(self.decorator, " ")?;
                    }
                    self.decorator.start_key()?;
                    self.decorator.write_all(k)?;
                    self.decorator.start_separator()?;
                    write!(self.decorator, ":")?;
                    self.decorator.start_whitespace()?;
                    write!(self.decorator, " ")?;
                    self.decorator.start_value()?;
                    self.decorator.write_all(v)?;

                    self.decorator.start_whitespace()?;
                    writeln!(self.decorator)?;
                }

                indent += 1;
            }

            Ok(indent)
        }
    }

    macro_rules! cs(
    ($s:expr, $k:expr, $v:expr) => {

        let mut k = vec!();
        let mut v = vec!();
        write!(&mut k, "{}", $k)?;
        write!(&mut v, "{}", $v)?;
        $s.buf.push((k, v));
    };
);

    impl<'a> super::ser::Serializer for CompactFormatSerializer<'a> {
        fn emit_none(&mut self, key: Key) -> super::Result {
            cs!(self, key, "None");
            Ok(())
        }
        fn emit_unit(&mut self, key: Key) -> super::Result {
            cs!(self, key, "()");
            Ok(())
        }

        fn emit_bool(&mut self, key: Key, val: bool) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }

        fn emit_char(&mut self, key: Key, val: char) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }

        fn emit_usize(&mut self, key: Key, val: usize) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_isize(&mut self, key: Key, val: isize) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }

        fn emit_u8(&mut self, key: Key, val: u8) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_i8(&mut self, key: Key, val: i8) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_u16(&mut self, key: Key, val: u16) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_i16(&mut self, key: Key, val: i16) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_u32(&mut self, key: Key, val: u32) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_i32(&mut self, key: Key, val: i32) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_f32(&mut self, key: Key, val: f32) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_u64(&mut self, key: Key, val: u64) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_i64(&mut self, key: Key, val: i64) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_f64(&mut self, key: Key, val: f64) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_str(&mut self, key: Key, val: &str) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
        fn emit_arguments(&mut self, key: Key, val: &fmt::Arguments) -> super::Result {
            cs!(self, key, val);
            Ok(())
        }
    }
    // }}}

    // {{{ CountingWriter
    // Wrapper for `Write` types that counts total bytes written.
    struct CountingWriter<'a> {
        wrapped: &'a mut io::Write,
        count: usize,
    }

    impl<'a> CountingWriter<'a> {
        fn new(wrapped: &'a mut io::Write) -> CountingWriter {
            CountingWriter {
                wrapped,
                count: 0,
            }
        }

        fn count(&self) -> usize {
            self.count
        }
    }

    impl<'a> io::Write for CountingWriter<'a> {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            self.wrapped.write(buf).map(|n| {
                self.count += n;
                n
            })
        }

        fn flush(&mut self) -> io::Result<()> {
            self.wrapped.flush()
        }

        fn write_all(&mut self, buf: &[u8]) -> io::Result<()> {
            self.wrapped.write_all(buf).map(|_| {
                self.count += buf.len();
            })
        }
    }
    // }}}

    // {{{ Timestamp
    /// Threadsafe timestamp formatting function type
    ///
    /// To satify `slog-rs` thread and unwind safety requirements, the
    /// bounds expressed by this trait need to satisfied for a function
    /// to be used in timestamp formatting.
    pub trait ThreadSafeTimestampFn:
        Fn(&mut io::Write) -> io::Result<()> + Send + Sync + UnwindSafe + RefUnwindSafe + 'static
    {
    }

    impl<F> ThreadSafeTimestampFn for F
    where
        F: Fn(&mut io::Write) -> io::Result<()> + Send + Sync,
        F: UnwindSafe + RefUnwindSafe + 'static,
        F: ?Sized,
    {
    }

    const TIMESTAMP_FORMAT: &str = "%b %d %H:%M:%S%.3f";

    /// Default local timezone timestamp function
    ///
    /// The exact format used, is still subject to change.
    pub fn timestamp_local(io: &mut io::Write) -> io::Result<()> {
        write!(io, "{}", chrono::Local::now().format(TIMESTAMP_FORMAT))
    }

    /// Default UTC timestamp function
    ///
    /// The exact format used, is still subject to change.
    pub fn timestamp_utc(io: &mut io::Write) -> io::Result<()> {
        write!(io, "{}", chrono::Utc::now().format(TIMESTAMP_FORMAT))
    }
    // }}}

    // {{{ Plain

    /// Plain (no-op) `Decorator` implementation
    ///
    /// This decorator doesn't do any coloring, and doesn't do any synchronization
    /// between threads, so is not `Sync`. It is however useful combined with
    /// `slog_async::Async` drain, as `slog_async::Async` uses only one thread,
    /// and thus requires only `Send` from `Drain`s it wraps.
    ///
    /// ```
    /// #[macro_use]
    /// extern crate slog;
    /// extern crate slog_term;
    /// extern crate slog_async;
    ///
    /// use slog::*;
    /// use slog_async::Async;
    ///
    /// fn main() {
    ///
    ///    let decorator = slog_term::PlainDecorator::new(std::io::stdout());
    ///    let drain = Async::new(
    ///            slog_term::FullFormat::new(decorator).build().fuse()
    ///        )
    ///        .build()
    ///        .fuse();
    /// }
    /// ```

    pub struct PlainDecorator<W>(RefCell<W>)
    where
        W: io::Write;

    impl<W> PlainDecorator<W>
    where
        W: io::Write,
    {
        /// Create `PlainDecorator` instance
        pub fn new(io: W) -> Self {
            PlainDecorator(RefCell::new(io))
        }
    }

    impl<W> Decorator for PlainDecorator<W>
    where
        W: io::Write,
    {
        fn with_record<F>(
            &self,
            _record: &Record,
            _logger_values: &OwnedKVList,
            f: F,
        ) -> io::Result<()>
        where
            F: FnOnce(&mut RecordDecorator) -> io::Result<()>,
        {
            f(&mut PlainRecordDecorator(&self.0))
        }
    }

    /// Record decorator used by `PlainDecorator`
    pub struct PlainRecordDecorator<'a, W: 'a>(&'a RefCell<W>)
    where
        W: io::Write;

    impl<'a, W> io::Write for PlainRecordDecorator<'a, W>
    where
        W: io::Write,
    {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            self.0.borrow_mut().write(buf)
        }

        fn flush(&mut self) -> io::Result<()> {
            self.0.borrow_mut().flush()
        }
    }

    impl<'a, W> Drop for PlainRecordDecorator<'a, W>
    where
        W: io::Write,
    {
        fn drop(&mut self) {
            let _ = self.flush();
        }
    }

    impl<'a, W> RecordDecorator for PlainRecordDecorator<'a, W>
    where
        W: io::Write,
    {
        fn reset(&mut self) -> io::Result<()> {
            Ok(())
        }
    }

    // }}}

    // {{{ PlainSync
    /// PlainSync `Decorator` implementation
    ///
    /// This implementation is exactly like `PlainDecorator` but it takes care
    /// of synchronizing writes to `io`.
    ///
    /// ```
    /// #[macro_use]
    /// extern crate slog;
    /// extern crate slog_term;
    ///
    /// use slog::*;
    ///
    /// fn main() {
    ///     let plain = slog_term::PlainSyncDecorator::new(std::io::stdout());
    ///     let root = Logger::root(
    ///         slog_term::FullFormat::new(plain).build().fuse(), o!()
    ///     );
    /// }
    /// ```
    pub struct PlainSyncDecorator<W>(sync::Arc<sync::Mutex<W>>)
    where
        W: io::Write;

    impl<W> PlainSyncDecorator<W>
    where
        W: io::Write,
    {
        /// Create `PlainSyncDecorator` instance
        pub fn new(io: W) -> Self {
            PlainSyncDecorator(sync::Arc::new(sync::Mutex::new(io)))
        }
    }

    impl<W> Decorator for PlainSyncDecorator<W>
    where
        W: io::Write,
    {
        fn with_record<F>(
            &self,
            _record: &Record,
            _logger_values: &OwnedKVList,
            f: F,
        ) -> io::Result<()>
        where
            F: FnOnce(&mut RecordDecorator) -> io::Result<()>,
        {
            f(&mut PlainSyncRecordDecorator {
                io: self.0.clone(),
                buf: vec![],
            })
        }
    }

    /// `RecordDecorator` used by `PlainSyncDecorator`
    pub struct PlainSyncRecordDecorator<W>
    where
        W: io::Write,
    {
        io: sync::Arc<sync::Mutex<W>>,
        buf: Vec<u8>,
    }

    impl<W> io::Write for PlainSyncRecordDecorator<W>
    where
        W: io::Write,
    {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            self.buf.write(buf)
        }

        fn flush(&mut self) -> io::Result<()> {
            if self.buf.is_empty() {
                return Ok(());
            }

            let mut io = self
                .io
                .lock()
                .map_err(|_| io::Error::new(io::ErrorKind::Other, "mutex locking error"))?;

            io.write_all(&self.buf)?;
            self.buf.clear();
            io.flush()
        }
    }

    impl<W> Drop for PlainSyncRecordDecorator<W>
    where
        W: io::Write,
    {
        fn drop(&mut self) {
            let _ = self.flush();
        }
    }

    impl<W> RecordDecorator for PlainSyncRecordDecorator<W>
    where
        W: io::Write,
    {
        fn reset(&mut self) -> io::Result<()> {
            Ok(())
        }
    }

    // }}}

    // {{{ TermDecorator

    /// Any type of a terminal supported by `term` crate
    // TODO: https://github.com/Stebalien/term/issues/70
    enum AnyTerminal {
        /// Stdout terminal
        Stdout(Box<term::StdoutTerminal>),
        /// Stderr terminal
        Stderr(Box<term::StderrTerminal>),
        FallbackStdout,
        FallbackStderr,
    }

    impl AnyTerminal {
        fn should_use_color(&self) -> bool {
            match *self {
                AnyTerminal::Stdout(_) => isatty::stdout_isatty(),
                AnyTerminal::Stderr(_) => isatty::stderr_isatty(),
                AnyTerminal::FallbackStdout => false,
                AnyTerminal::FallbackStderr => false,
            }
        }
    }

    /// `TermDecorator` builder
    pub struct TermDecoratorBuilder {
        use_stderr: bool,
        color: Option<bool>,
    }

    impl TermDecoratorBuilder {
        fn new() -> Self {
            TermDecoratorBuilder {
                use_stderr: true,
                color: None,
            }
        }

        /// Output to `stderr`
        pub fn stderr(mut self) -> Self {
            self.use_stderr = true;
            self
        }

        /// Output to `stdout`
        pub fn stdout(mut self) -> Self {
            self.use_stderr = false;
            self
        }

        /// Force colored output
        pub fn force_color(mut self) -> Self {
            self.color = Some(true);
            self
        }

        /// Force colored output
        pub fn force_plain(mut self) -> Self {
            self.color = Some(false);
            self
        }

        /// Try to build `TermDecorator`
        ///
        /// Unlike `build` this will not fall-back to raw `stdout`/`stderr`
        /// if it wasn't able to use terminal and its features directly
        /// (eg. if `TERM` env. was not set).
        pub fn try_build(self) -> Option<TermDecorator> {
            let io = if self.use_stderr {
                term::stderr().map(AnyTerminal::Stderr)
            } else {
                term::stdout().map(AnyTerminal::Stdout)
            };

            io.map(|io| {
                let use_color = self.color.unwrap_or_else(|| io.should_use_color());
                TermDecorator {
                    use_color,
                    term: RefCell::new(io),
                }
            })
        }

        /// Build `TermDecorator`
        ///
        /// Unlike `try_build` this it will fall-back to using plain `stdout`/`stderr`
        /// if it wasn't able to use terminal directly.
        pub fn build(self) -> TermDecorator {
            let io = if self.use_stderr {
                term::stderr()
                    .map(AnyTerminal::Stderr)
                    .unwrap_or(AnyTerminal::FallbackStderr)
            } else {
                term::stdout()
                    .map(AnyTerminal::Stdout)
                    .unwrap_or(AnyTerminal::FallbackStdout)
            };

            let use_color = self.color.unwrap_or_else(|| io.should_use_color());
            TermDecorator {
                term: RefCell::new(io),
                use_color,
            }
        }
    }

    /// `Decorator` implemented using `term` crate
    ///
    /// This decorator will add nice formatting to the logs it's outputting. It's
    /// based on `term` crate.
    ///
    /// It does not deal with serialization so is `!Sync`. Run in a separate thread
    /// with `slog_async::Async`.
    pub struct TermDecorator {
        term: RefCell<AnyTerminal>,
        use_color: bool,
    }

    impl TermDecorator {
        /// Start building `TermDecorator`
        pub fn new() -> TermDecoratorBuilder {
            TermDecoratorBuilder::new()
        }

        /// `Level` color
        ///
        /// Standard level to Unix color conversion used by `TermDecorator`
        pub fn level_to_color(level: super::Level) -> u16 {
            match level {
                Level::Critical => 5,
                Level::Error => 1,
                Level::Warning => 3,
                Level::Info => 2,
                Level::Debug => 6,
                Level::Trace => 4,
            }
        }
    }

    impl Decorator for TermDecorator {
        fn with_record<F>(
            &self,
            record: &Record,
            _logger_values: &OwnedKVList,
            f: F,
        ) -> io::Result<()>
        where
            F: FnOnce(&mut RecordDecorator) -> io::Result<()>,
        {
            let mut term = self.term.borrow_mut();
            let mut deco = TermRecordDecorator {
                term: &mut *term,
                level: record.level(),
                use_color: self.use_color,
            };
            {
                f(&mut deco)
            }
        }
    }

    /// Record decorator used by `TermDecorator`
    pub struct TermRecordDecorator<'a> {
        term: &'a mut AnyTerminal,
        level: super::Level,
        use_color: bool,
    }

    impl<'a> io::Write for TermRecordDecorator<'a> {
        fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
            match self.term {
                AnyTerminal::Stdout(ref mut term) => term.write(buf),
                AnyTerminal::Stderr(ref mut term) => term.write(buf),
                AnyTerminal::FallbackStdout => std::io::stdout().write(buf),
                AnyTerminal::FallbackStderr => std::io::stderr().write(buf),
            }
        }

        fn flush(&mut self) -> io::Result<()> {
            match self.term {
                AnyTerminal::Stdout(ref mut term) => term.flush(),
                AnyTerminal::Stderr(ref mut term) => term.flush(),
                AnyTerminal::FallbackStdout => std::io::stdout().flush(),
                AnyTerminal::FallbackStderr => std::io::stderr().flush(),
            }
        }
    }

    impl<'a> Drop for TermRecordDecorator<'a> {
        fn drop(&mut self) {
            let _ = self.flush();
        }
    }

    fn term_error_to_io_error(e: term::Error) -> io::Error {
        match e {
            term::Error::Io(e) => e,
            e => io::Error::new(io::ErrorKind::Other, format!("term error: {}", e)),
        }
    }

    impl<'a> RecordDecorator for TermRecordDecorator<'a> {
        fn reset(&mut self) -> io::Result<()> {
            if !self.use_color {
                return Ok(());
            }
            match self.term {
                AnyTerminal::Stdout(ref mut term) => term.reset(),
                AnyTerminal::Stderr(ref mut term) => term.reset(),
                AnyTerminal::FallbackStdout | &mut AnyTerminal::FallbackStderr => Ok(()),
            }
            .map_err(term_error_to_io_error)
        }

        fn start_level(&mut self) -> io::Result<()> {
            if !self.use_color {
                return Ok(());
            }
            let color = TermDecorator::level_to_color(self.level);
            match self.term {
                AnyTerminal::Stdout(ref mut term) => term.fg(color as term::color::Color),
                AnyTerminal::Stderr(ref mut term) => term.fg(color as term::color::Color),
                AnyTerminal::FallbackStdout | &mut AnyTerminal::FallbackStderr => Ok(()),
            }
            .map_err(term_error_to_io_error)
        }

        fn start_key(&mut self) -> io::Result<()> {
            if !self.use_color {
                return Ok(());
            }
            match self.term {
                AnyTerminal::Stdout(ref mut term) => {
                    if term.supports_attr(term::Attr::Bold) {
                        term.attr(term::Attr::Bold)
                    } else {
                        term.fg(term::color::BRIGHT_WHITE)
                    }
                }
                AnyTerminal::Stderr(ref mut term) => {
                    if term.supports_attr(term::Attr::Bold) {
                        term.attr(term::Attr::Bold)
                    } else {
                        term.fg(term::color::BRIGHT_WHITE)
                    }
                }
                AnyTerminal::FallbackStdout | AnyTerminal::FallbackStderr => Ok(()),
            }
            .map_err(term_error_to_io_error)
        }

        fn start_msg(&mut self) -> io::Result<()> {
            // msg is just like key
            self.start_key()
        }
    }

    // }}}

    // {{{ TestStdoutWriter
    /// Replacement for `std::io::stdout()` for when output capturing by rust's test
    /// harness is required.
    pub struct TestStdoutWriter;

    impl io::Write for TestStdoutWriter {
        fn write(&mut self, data: &[u8]) -> io::Result<usize> {
            print!(
                "{}",
                std::str::from_utf8(data)
                    .map_err(|x| io::Error::new(io::ErrorKind::InvalidData, x))?
            );
            Ok(data.len())
        }
        fn flush(&mut self) -> io::Result<()> {
            io::stdout().flush()
        }
    }
    // }}}

    // {{{ Helpers
    /// Create a `CompactFormat` drain with default settings
    pub fn term_compact() -> CompactFormat<TermDecorator> {
        let decorator = TermDecorator::new().build();
        CompactFormat::new(decorator).build()
    }

    /// Create a `FullFormat` drain with default settings
    pub fn term_full() -> FullFormat<TermDecorator> {
        let decorator = TermDecorator::new().build();
        FullFormat::new(decorator).build()
    }

    // }}}

    // vim: foldmethod=marker foldmarker={{{,}}}
}

/* cSpell:enable */
