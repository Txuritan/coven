use actix_web::{
    middleware::{Middleware, Started},
    Result,
};

use crate::routes::prelude::*;

#[allow(dead_code, unused_variables)]
pub(crate) struct ApiHeader;

impl<S> Middleware<S> for ApiHeader {
    #[allow(dead_code, unused_variables)]
    fn start(&self, req: &HttpRequest<S>) -> Result<Started> {
        Ok(Started::Response(HttpResponse::Unauthorized().finish()))
    }
}
