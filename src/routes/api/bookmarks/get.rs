use std::borrow::Cow;

use serde_derive::{Deserialize, Serialize};

use crate::{models::tables::Bookmark, routes::prelude::*};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct GetOnePath {
    id: i64,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub(crate) struct GetOneQuery {
    #[serde(default = "Default::default")]
    content: Option<Cow<'static, str>>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) enum Get {
    Id(i64),
    Ids(Vec<i64>),
}

impl Get {
    pub(crate) fn get((state, query): (State<Coven>, Query<GetSome>)) -> impl Responder {
        state
            .database
            .send(query.into_inner())
            .map_err(Error::from)
            .and_then(|res| {
                res.map_err(Error::from)
                    .and_then(|json| Ok(HttpResponse::Ok().json(json)))
            })
            .responder()
    }

    pub(crate) fn get_id(
        (state, path, query): (State<Coven>, Path<GetOnePath>, Query<GetOneQuery>),
    ) -> impl Responder {
        state
            .database
            .send(GetOne {
                path: path.into_inner(),
                query: query.into_inner(),
            })
            .map_err(Error::from)
            .and_then(|res| {
                res.map_err(Error::from)
                    .and_then(|json| Ok(HttpResponse::Ok().json(json)))
            })
            .responder()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct GetSome {
    #[serde(default = "default_offset")]
    offset: i64,
    #[serde(default = "default_limit")]
    limit: i64,
    #[serde(default = "Default::default")]
    content: Option<Cow<'static, str>>,
}

fn default_offset() -> i64 {
    0
}

fn default_limit() -> i64 {
    50
}

impl Message for GetSome {
    type Result = Result<Vec<Bookmark>, Error>;
}

impl Handler<GetSome> for Database {
    type Result = Result<Vec<Bookmark>, Error>;

    fn handle(&mut self, msg: GetSome, _: &mut Self::Context) -> Self::Result {
        let conn = self.pool.get()?;

        let bookmarks = Bookmark::get_all(
            &conn,
            msg.offset,
            msg.limit,
            msg.content.is_some() && msg.content != Some(Cow::from("false")),
        )?;

        Ok(bookmarks)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct GetOne {
    path: GetOnePath,
    query: GetOneQuery,
}

impl Message for GetOne {
    type Result = Result<Bookmark, Error>;
}

impl Handler<GetOne> for Database {
    type Result = Result<Bookmark, Error>;

    fn handle(&mut self, msg: GetOne, _: &mut Self::Context) -> Self::Result {
        let conn = self.pool.get()?;

        let bookmark = Bookmark::get(
            &conn,
            msg.path.id,
            msg.query.content.is_some() && msg.query.content != Some(Cow::from("false")),
        )?;

        Ok(bookmark)
    }
}
