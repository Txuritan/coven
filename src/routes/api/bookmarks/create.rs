use std::borrow::Cow;
use std::str;

use actix_web::client::ClientRequest;
use serde_derive::{Deserialize, Serialize};

use crate::{
    models::{go::*, tables::Bookmark},
    routes::prelude::*,
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub(crate) struct Create {
    #[serde(default = "Default::default")]
    tags: Cow<'static, str>,
    title: Option<Cow<'static, str>>,
    url: Cow<'static, str>,
}

impl Create {
    // TODO: support vectors and not just a single go
    pub(crate) fn post((state, json): (State<Coven>, Json<Create>)) -> impl Responder {
        Self::get_page(
            &format!("http://localhost:{}/json", state.settings.server.port + 1),
            &json.url,
        )
        .and_then(move |body| {
            state
                .database
                .send(CreatePage {
                    body,
                    json: json.into_inner(),
                })
                .map_err(Error::from)
                .and_then(|res| {
                    res.map_err(Error::from)
                        .and_then(|json| Ok(HttpResponse::Ok().json(json)))
                })
        })
        .responder()
    }

    fn get_page(go: &str, url: &str) -> impl Future<Item = GoResponse, Error = Error> {
        result(ClientRequest::post(go).json(GoRequest {
            url: url.to_string(),
        }))
        .map_err(Error::from)
        .and_then(|client| {
            client.send().map_err(Error::from).and_then(|res| {
                res.body().from_err().and_then(|body| {
                    result(GoResponse::from_bytes(&body))
                        .map_err(Error::from)
                        .and_then(|res| result(Ok(res)))
                })
            })
        })
    }
}

pub(crate) struct CreatePage {
    body: GoResponse,
    json: Create,
}

impl Message for CreatePage {
    type Result = Result<Bookmark, Error>;
}

impl Handler<CreatePage> for Database {
    type Result = Result<Bookmark, Error>;

    fn handle(&mut self, msg: CreatePage, _: &mut Self::Context) -> Self::Result {
        let conn = self.pool.get()?;

        // No one will have 1mb of tags... I hope
        let tags = msg
            .json
            .tags
            .split(", ")
            .flat_map(|tags| tags.split(','))
            .collect::<Vec<&str>>();

        let bookmark = Bookmark::create(&conn, &msg.json.url, &tags, &msg.json.title, msg.body)?;

        Ok(bookmark)
    }
}
