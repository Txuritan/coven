pub(crate) mod api;

pub(crate) mod prelude;

use self::{
    api::{bookmarks, Api},
    prelude::*,
};

pub(crate) fn app(database: Database, settings: Settings) -> App<Coven> {
    App::with_state(Coven {
        database: SyncArbiter::start(3, move || database.clone()),
        settings,
    })
    .middleware(middleware::Logger::default())
    .scope("/api", |s| {
        s.resource("", |r| r.get().f(Api::get))
            .nested("/bookmarks", |n| {
                n.resource("", |r| {
                    r.get().with(bookmarks::Get::get);
                    r.post().with(bookmarks::Create::post)
                })
                .resource("/{id}", |r| r.get().with(bookmarks::Get::get_id))
            })
    })
}
