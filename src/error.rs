use std::borrow::Cow;
use std::str::Utf8Error;

use actix_web::{
    actix::MailboxError,
    client::SendRequestError,
    error::{Error as ActixError, JsonPayloadError, PayloadError, ResponseError},
    http::StatusCode,
    HttpResponse,
};
use config::ConfigError;
use quick_error::quick_error;
use r2d2::Error as R2D2Error;
use rusqlite::Error as SQLiteError;
use serde_json::error::Error as JsonError;

quick_error! {
    #[derive(Debug)]
    pub enum Error {
        Utf8(err: Utf8Error) {
            from()
            description("utf8 error")
            display("UTF8 error: {}", err)
        }

        Actix(err: ActixError) {
            from()
            description("actix error")
            display("Actix error: {}", err)
        }
        JsonPayload(err: JsonPayloadError) {
            from()
            description("json payload error")
            display("Json Payload error: {}", err)
        }
        Payload(err: PayloadError) {
            from()
            description("payload error")
            display("Payload error: {}", err)
        }
        Mailbox(err: MailboxError) {
            from()
            description("mailbox error")
            display("Mailbox error: {}", err)
        }
        SendRequest(err: SendRequestError) {
            from()
            description("send request error")
            display("Send Request error: {}", err)
        }

        R2D2(err: R2D2Error) {
            from()
            description("r2d2 error")
            display("R2D2 error: {}", err)
        }
        SQLite(err: SQLiteError) {
            from()
            description("sqlite error")
            display("SQLite error: {}", err)
        }

        Config(err: ConfigError) {
            from()
            description("config error")
            display("Config error: {}", err)
            cause(err)
        }

        Json(err: JsonError) {
            from()
            description("json error")
            display("Json error: {}", err)
            cause(err)
        }

        GoResponse(msg: Cow<'static, str>) {
            description("error response from go server")
            display("Error: {}, from Go server", msg)
        }

        WrongDatabaseAccess(msg: &'static str) {
            description("tried to access database from the given scope")
            display("Tried to access database `{}` from outside the given scope", msg)
        }
    }
}

impl ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        match *self {
            Error::JsonPayload(ref err) => err.error_response(),
            Error::R2D2(ref err) => {
                error!("{}", err);

                HttpResponse::new(StatusCode::INTERNAL_SERVER_ERROR)
            }
            _ => HttpResponse::new(StatusCode::BAD_REQUEST),
        }
    }
}
