package main

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"net/http"
	"net/url"

	"github.com/RadhiFadlillah/go-readability"
)

type jsonError struct {
	Error string
}

type request struct {
	URL string
}

func main() {
	port := os.Args[1]

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var req request

		if r.Body == nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(jsonError{"No request body"})
			return
		}

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(jsonError{err.Error()})
			return
		}

		parsedURL, err := url.Parse(req.URL)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(jsonError{err.Error()})
			return
		}

		article, err := readability.FromURL(parsedURL, 5*time.Second)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(jsonError{err.Error()})
			return
		}

		json.NewEncoder(w).Encode(article)
	})

	err := http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
	if err != nil {
		panic(err)
	}
}
